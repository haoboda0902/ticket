package com.hbd.ticketscheck.orderupdate;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.erqode.ErQodeActivity;
import com.hbd.ticketscheck.main.MainActivity;
import com.hbd.ticketscheck.order.bean.OrderBean;
import com.hbd.ticketscheck.orderupdate.adapter.OrderUpdateAdapter;
import com.hbd.ticketscheck.orderupdate.bean.OrderItems;
import com.hbd.ticketscheck.orderupdate.ivew.OrderUpdateView;
import com.hbd.ticketscheck.orderupdate.presenter.OrderUpdatePresenter;
import com.hbd.ticketscheck.view.LoadingDialog;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class OrderUpdateActivity extends BaseMvpActivity<OrderUpdatePresenter> implements OrderUpdateView, EasyPermissions.PermissionCallbacks  {

    private static final int REQUEST_CODE_QRCODE_PERMISSIONS = 1;

    private ImageView back;
    private RecyclerView recyclerView;
    private EditText orderNum,people,phone,area,address;
    private TextView copy,save;
    private Button btnErqode;

    private OrderBean.OrdersBean ordersBean;

    private OrderUpdateAdapter adapter;
    private  String code;
    private LoadingDialog dialog;

    @Override
    protected void initView() {

        recyclerView = findViewById(R.id.ry_order_update);
        orderNum = findViewById(R.id.order_num);
        people = findViewById(R.id.order_people);
        phone = findViewById(R.id.order_phone);
        area = findViewById(R.id.order_area);
        address = findViewById(R.id.order_address);
        back = findViewById(R.id.back);
        btnErqode = findViewById(R.id.btn_erqode);
        copy = findViewById(R.id.order_copy);
        save = findViewById(R.id.order_save);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OrderUpdateActivity.this.finish();
            }
        });
        btnErqode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                requestCodeQRCodePermissions();

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commit();
            }
        });
        copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                copyOrderInfo();
            }
        });
        getIntentData();
    }

    private void copyOrderInfo() {

        // 获取系统剪贴板
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        // 创建一个剪贴数据集，包含一个普通文本数据条目（需要复制的数据）
        if(ordersBean != null) {
            StringBuilder stringBuilder = new StringBuilder();

            if(orderNum.getText().toString().trim().length() > 0){
                stringBuilder.append("快递单号：" + orderNum.getText().toString().trim());
            }

            stringBuilder.append("收件人：" + ordersBean.getReceiverName());
            stringBuilder.append("  手机号：" + ordersBean.getReceiverMobile());
            stringBuilder.append("   地址：" + ordersBean.getReceiverAddress());
            ClipData clipData = ClipData.newPlainText(null, stringBuilder.toString());
            // 把数据集设置（复制）到剪贴板
            clipboard.setPrimaryClip(clipData);
            Toast.makeText(context, "复制成功", Toast.LENGTH_SHORT).show();
        }
    }

    private void getIntentData() {

        ordersBean = (OrderBean.OrdersBean) getIntent().getSerializableExtra("data");

        people.setText(ordersBean.getReceiverName());
        phone.setText(ordersBean.getReceiverMobile());
        address.setText(ordersBean.getReceiverAddress());
        mPresenter.getProductOrderItemForApp(ordersBean.getOrderNo());
    }

    private void commit() {

        if(orderNum.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入运单号",Toast.LENGTH_SHORT).show();
            return;
        }
        mPresenter.save(ordersBean.getOrderNo(),orderNum.getText().toString().trim());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order_update;
    }

    @Override
    protected OrderUpdatePresenter createPresenter() {
        return new OrderUpdatePresenter(this);
    }

    @Override
    public void saveSuccess() {
            activity.finish();
    }

    @Override
    public void saveError(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getOrder(OrderItems orderItems) {
        adapter = new OrderUpdateAdapter(context,orderItems.getOrderItems());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void getOrderError(String msg) {

    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(context,"Loading...");
        dialog.show();
    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1000){
            if(data != null) {
                code = data.getStringExtra("data");
                orderNum.setText(code);
            }
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        // 授予权限

        gotoLogin();

    }

    private void gotoLogin() {
        Intent intent = new Intent(activity, ErQodeActivity.class);
        intent.putExtra("data",true);
        startActivityForResult(intent,1000);

    }


    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        // 请求权限被拒
        Toast.makeText(this,"请在设置里同意用户权限",Toast.LENGTH_SHORT).show();
    }

    @AfterPermissionGranted(REQUEST_CODE_QRCODE_PERMISSIONS)
    private void requestCodeQRCodePermissions() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, "扫描二维码需要打开相机和闪光灯的权限", REQUEST_CODE_QRCODE_PERMISSIONS, perms);
        }else{
            gotoLogin();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
