package com.hbd.ticketscheck.shop.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpFragment;
import com.hbd.network.basepresenter.BasePresenter;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.LampUpdateActivity;
import com.hbd.ticketscheck.shop.adapter.LampAdapter;
import com.hbd.ticketscheck.shop.adapter.LampDateAdapter;
import com.hbd.ticketscheck.shop.bean.LampListBean;
import com.hbd.ticketscheck.shop.iview.LampView;
import com.hbd.ticketscheck.shop.presenter.LampPresenter;
import com.hbd.ticketscheck.view.LoadingDialog;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LampFragment extends BaseMvpFragment<LampPresenter> implements LampView {

    private SmartRefreshLayout smartRefreshLayout;
    private RecyclerView rvTitle;
    private RecyclerView rvData;
    private Button btnAdd;

    private LampDateAdapter adapter;
    private LampAdapter lampAdapter;
    private int pageNo = 1;
    private int pageSize = 10;
    private int total = 0;
    private String showDate ="";
    private List<LampListBean.GoodsListBean> list;
    private LoadingDialog dialog;

    private int updatePosition;

    @Override
    protected void initView(View view) {
        rvTitle =   view.findViewById(R.id.lamp_time);
        rvData = view.findViewById(R.id.lamp_list);
        btnAdd = view.findViewById(R.id.lamp_add);
        smartRefreshLayout = view.findViewById(R.id.smartrefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {

                pageNo = 1;
                smartRefreshLayout.setEnableLoadMore(true);//启用加载
                initList(showDate);
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                initList(showDate);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LampUpdateActivity.class);
                LampListBean.GoodsListBean goodsListBean = new LampListBean.GoodsListBean();
                goodsListBean.setId(-1);
                goodsListBean.setShowDate(showDate);
                intent.putExtra("data",goodsListBean);
                startActivity(intent);
            }
        });
        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getGoodsDateList();
    }

    private void initData() {
        list = new ArrayList<>();
        lampAdapter = new LampAdapter(list,getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvData.setLayoutManager(linearLayoutManager);
        rvData.setAdapter(lampAdapter);

       lampAdapter.setOnItemClick(new LampAdapter.OnItemClick() {
           @Override
           public void onItemUpdateClick(int position) {
               Intent intent = new Intent(getActivity(), LampUpdateActivity.class);

               intent.putExtra("data",list.get(position));
               startActivity(intent);
           }

           @Override
           public void onItemUpDownClick(int position) {

               AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
               alertDialog.setTitle("确定执行上下架操作吗").setPositiveButton("取消", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {

                   }
               }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                   @Override
                   public void onClick(DialogInterface dialogInterface, int i) {
                       updatePosition = position;
                       Map<String,Object> param = new HashMap<>();
                       if(list.get(position).getIsPut() == 0){
                           param.put("isPut",1);
                       }else{
                           param.put("isPut",0);
                       }
                       param.put("id",list.get(position).getId());
                       mPresenter.updateOrAdd(param);
                   }
               }).show();


           }
       });

    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_lamp;
    }

    @Override
    protected LampPresenter createPresenter() {
        return new LampPresenter(this);
    }

    @Override
    public void getDate(List<String> data) {
        adapter = new LampDateAdapter(data,getContext());
        adapter.setOnItemClickLisenter(new LampDateAdapter.OnItemClickLisenter() {
            @Override
            public void onClick(int position) {
                pageNo = 1;
                showDate = data.get(position);
                initList(showDate);
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        rvTitle.setLayoutManager(linearLayoutManager);
        rvTitle.setAdapter(adapter);
        showDate =  data.get(0);
        initList(showDate);
    }

    private void initList(String datum) {

        mPresenter.getGoodsListByApp(pageNo,pageSize,datum);
    }

    @Override
    public void getList(LampListBean listBeans) {
        total = listBeans.getTotal();
        if(pageNo == 1){
            list.clear();
            smartRefreshLayout.finishRefresh();
        }else {
            smartRefreshLayout.finishLoadMore();
        }
        pageNo++;
        list.addAll(listBeans.getGoodsList());
        lampAdapter.notifyDataSetChanged();
        if(list.size() == total){
            smartRefreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void upDateSuccess() {
        if(list.get(updatePosition).getIsPut() == 0){
            list.get(updatePosition).setIsPut(1);
        }else{
            list.get(updatePosition).setIsPut(0);
        }
        lampAdapter.notifyItemChanged(updatePosition);
    }

    @Override
    public void upDateError(String msg) {

    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this.getContext(),"玩命加载中...");
        dialog.show();
    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }
}
