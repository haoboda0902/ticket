package com.hbd.ticketscheck.main;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpActivity;
import com.hbd.network.basepresenter.BasePresenter;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.company.CompanyActivity;
import com.hbd.ticketscheck.main.adapter.MainActionAdapter;
import com.hbd.ticketscheck.main.bean.ActionBean;
import com.hbd.ticketscheck.order.OrderActivity;
import com.hbd.ticketscheck.orderstatistics.OrderStatisticsActivity;
import com.hbd.ticketscheck.shop.LampUpdateActivity;
import com.hbd.ticketscheck.shop.ShopUpdateActivity;
import com.hbd.ticketscheck.shoptype.ShopTypeActivity;
import com.hbd.ticketscheck.store.StoreActivity;

import java.util.ArrayList;
import java.util.List;

public class NewMainActivity extends BaseMvpActivity {

    private ImageView btnAction;
    private RecyclerView ryActionList;
    private RelativeLayout ryMain;

    private List<ActionBean> list = new ArrayList<>();
    private MainActionAdapter actionAdapter;

    @Override
    protected void initView() {
        btnAction = findViewById(R.id.btn_main_action);
        ryActionList = findViewById(R.id.main_recyclerview);
        ryMain = findViewById(R.id.ry_main);
        btnAction.setVisibility(View.VISIBLE);
        ryActionList.setVisibility(View.GONE);
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ryActionList.setVisibility(View.VISIBLE);
                btnAction.setVisibility(View.GONE);
            }
        });
        ryMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnAction.setVisibility(View.VISIBLE);
                ryActionList.setVisibility(View.GONE);
            }
        });

        actionAdapter = new MainActionAdapter(this,list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        ryActionList.setLayoutManager(linearLayoutManager);
        ryActionList.setAdapter(actionAdapter);
        actionAdapter.setOnItemClickListener(new MainActionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent;
                switch (position){
                    case 0:
                        intent = new Intent(NewMainActivity.this, OrderActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(NewMainActivity.this, ShopUpdateActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(NewMainActivity.this, ShopTypeActivity.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(NewMainActivity.this, CompanyActivity.class);
                        startActivity(intent);

                        break;
                    case 4:
                        intent = new Intent(NewMainActivity.this, OrderStatisticsActivity.class);
                        startActivity(intent);

                        break;

                    case 5:
                        intent = new Intent(NewMainActivity.this, StoreActivity.class);
                        startActivity(intent);
                        break;

                    case 6:
                        intent = new Intent(NewMainActivity.this, MainActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        });
        initData();
    }

    private void initData() {

        ActionBean actionBean1 = new ActionBean(0,"订单处理");
        list.add(actionBean1);
        ActionBean actionBean2 = new ActionBean(1,"商品管理");
        list.add(actionBean2);
        ActionBean actionBean3 = new ActionBean(2,"分类管理");
        list.add(actionBean3);
        ActionBean actionBean4 = new ActionBean(3,"合作管理");
        list.add(actionBean4);
        ActionBean actionBean5 = new ActionBean(4,"订单统计");
        list.add(actionBean5);
        ActionBean actionBean6 = new ActionBean(5,"仓库管理");
        list.add(actionBean6);
        ActionBean actionBean7 = new ActionBean(6,"验票核销");
        list.add(actionBean7);
        actionAdapter.notifyDataSetChanged();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_new_main;
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }
}
