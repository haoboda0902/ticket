package com.hbd.ticketscheck.shop.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.shop.bean.MetrailBean;


public interface MaterialView extends IBaseView {

    void getDataSuccess(MetrailBean metrailBean);


    void upDateSuccess();

    void upDateError(String msg);
}
