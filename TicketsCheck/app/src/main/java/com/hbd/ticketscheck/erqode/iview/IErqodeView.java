package com.hbd.ticketscheck.erqode.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.erqode.bean.CheckCodeBean;

public interface IErqodeView extends IBaseView {

    void checkSuccess(CheckCodeBean userBean);

    void checkError(String msg);

}
