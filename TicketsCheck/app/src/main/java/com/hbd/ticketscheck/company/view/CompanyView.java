package com.hbd.ticketscheck.company.view;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.company.bean.CompanyBean;

public interface CompanyView extends IBaseView {

    void getData(CompanyBean companyBean);

    void updateSuccess();

    void updateError(String msg);
}
