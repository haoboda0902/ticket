package com.hbd.ticketscheck.company.bean;

import java.io.Serializable;
import java.util.List;

public class CompanyBean implements Serializable {


    /**
     * total : 46
     * productList : [{"contectName":"","createDate":"","createUser":"","desc":"","id":14,"travelMobile":"18035005539","travelName":"五台山风景名胜区名途旅行社有限公司","travelNo":"dzwts00112","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":15,"travelMobile":"18535007170","travelName":"忻州五台山风景名胜区银河旅行社有限公司","travelNo":"dzwts00113","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":16,"travelMobile":"18536388203","travelName":"忻州五台山风景名胜区纵横旅行社有限公司","travelNo":"dzwts00114","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":17,"travelMobile":"13935015543","travelName":"五台县五台山运通旅行社有限公司","travelNo":"dzwts00115","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":18,"travelMobile":"18295820410","travelName":"忻州市五台山风景区泛华旅行社有限公司","travelNo":"dzwts00116","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":19,"travelMobile":"18635011919","travelName":"忻州市五台山风景名胜区祥缘旅行社有限公司","travelNo":"dzwts00117","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":20,"travelMobile":"18636065600","travelName":"五台县五台山金桥商务旅行时有限公司","travelNo":"dzwts00118","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":21,"travelMobile":"13803449562","travelName":"忻州五台山风景名胜区假日旅行社有限公司","travelNo":"dzwts00119","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":22,"travelMobile":"15235379669","travelName":"忻州市五台山风景名胜区金马旅行社","travelNo":"dzwts00120","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":23,"travelMobile":"15835062389","travelName":"五台县五台山日升达旅行社有限公司","travelNo":"dzwts00121","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":24,"travelMobile":"15703500700","travelName":"五台县五台山日月星旅行社有限公司","travelNo":"dzwts00122","updateDate":"","updateUser":""},{"contectName":"","createDate":"","createUser":"","desc":"","id":25,"travelMobile":"15235024786","travelName":"忻州五台山海外旅行社有限公司","travelNo":"dzwts00123","updateDate":"","updateUser":""}]
     */

    private int total;
    private List<ProductListBean> productList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ProductListBean> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductListBean> productList) {
        this.productList = productList;
    }

    public static class ProductListBean {
        /**
         * contectName :
         * createDate :
         * createUser :
         * desc :
         * id : 14
         * travelMobile : 18035005539
         * travelName : 五台山风景名胜区名途旅行社有限公司
         * travelNo : dzwts00112
         * updateDate :
         * updateUser :
         */

        private String contectName;
        private String createDate;
        private String createUser;
        private String desc;
        private int id;
        private String travelMobile;
        private String travelName;
        private String travelNo;
        private String updateDate;
        private String updateUser;

        public String getContectName() {
            return contectName;
        }

        public void setContectName(String contectName) {
            this.contectName = contectName;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTravelMobile() {
            return travelMobile;
        }

        public void setTravelMobile(String travelMobile) {
            this.travelMobile = travelMobile;
        }

        public String getTravelName() {
            return travelName;
        }

        public void setTravelName(String travelName) {
            this.travelName = travelName;
        }

        public String getTravelNo() {
            return travelNo;
        }

        public void setTravelNo(String travelNo) {
            this.travelNo = travelNo;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }
    }
}
