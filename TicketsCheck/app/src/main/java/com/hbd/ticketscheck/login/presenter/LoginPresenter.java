package com.hbd.ticketscheck.login.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.login.bean.UserBean;
import com.hbd.ticketscheck.login.iview.LoginView;
import com.tencent.mmkv.MMKV;

import java.util.HashMap;
import java.util.Map;

public class LoginPresenter extends MyBasePresenter<LoginView> {

    public LoginPresenter(LoginView mainView) {
        attachView(mainView);
    }
    public void login(String moblie,String pwd,String verCode){
        Map<String,String> params = new HashMap<>();
        params.put("userName",moblie);
        params.put("passwd",pwd);
        params.put("verCode",verCode);
        iView.showLoading();
        subscribe(apiServer.login(params), new BaseObserver<BaseModel<UserBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<UserBean> baseModel) {
                LogUtils.d(baseModel.toString());
                UserBean userBean = baseModel.getBody();

                if(userBean != null) {
                    MMKV mmkv = MMKV.defaultMMKV();
                    mmkv.encode("token", userBean.getToken());
                    mmkv.encode("userid", userBean.getUserid());
                    iView.login();
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
            }
        });
    }

    public void getVerCode(String moblie,String pwd){
        Map<String,String> params = new HashMap<>();
        params.put("userName",moblie);
        params.put("passwd",pwd);
        iView.showLoading();
        subscribe(apiServer.getVerCode(params), new BaseObserver<BaseModel>(iView) {

            @Override
            public void onSuccess(BaseModel baseModel) {
                iView.codeSuccess();
            }

            @Override
            public void onError(String msg) {
                iView.codeError(msg);
                LogUtils.d(msg.toString());
            }
        });
    }
}
