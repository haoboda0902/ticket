package com.hbd.ticketscheck.welcome.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.login.bean.UserBean;
import com.hbd.ticketscheck.welcome.iview.IWelcomeView;
import com.tencent.mmkv.MMKV;

import java.util.HashMap;
import java.util.Map;

public class WelcomePresenter extends MyBasePresenter<IWelcomeView> {

    public WelcomePresenter(IWelcomeView iWelcomeView) {
        attachView(iWelcomeView);
    }

    public void loginToken(String  token,String userid){
        Map<String,String> params = new HashMap<>();
//        params.put("token",token);
//        params.put("userid",userid);
        iView.showLoading();
        subscribe(apiServer.loginToken(params), new BaseObserver<BaseModel<UserBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<UserBean> baseModel) {
                LogUtils.d(baseModel.toString());
                UserBean userBean = baseModel.getBody();
                if(userBean != null) {
                    MMKV mmkv = MMKV.defaultMMKV();
                    mmkv.encode("token", userBean.getToken());
                    mmkv.encode("userid", userBean.getUserid());
                    iView.loginSuccess();
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.loginError(msg);
            }
        });
    }
}
