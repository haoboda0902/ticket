package com.hbd.ticketscheck.erqode.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.erqode.bean.CheckCodeBean;
import com.hbd.ticketscheck.erqode.iview.IErqodeView;

import java.util.HashMap;
import java.util.Map;

public class ErqodePresenter extends MyBasePresenter<IErqodeView> {

    public ErqodePresenter(IErqodeView iWelcomeView) {
        attachView(iWelcomeView);
    }

    public void check(String  token,String userid,String code){
        Map<String,String> params = new HashMap<>();
        params.put("token",token);
        params.put("userid",userid);
        params.put("checkCode",code);
        iView.showLoading();
        subscribe(apiServer.getOrderByOrderNo(params), new BaseObserver<BaseModel<CheckCodeBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<CheckCodeBean> baseModel) {
                LogUtils.d(baseModel.toString());
                CheckCodeBean userBean = baseModel.getBody();
                if(userBean != null) {
                    iView.checkSuccess(userBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.checkError(msg);
            }
        });
    }
}
