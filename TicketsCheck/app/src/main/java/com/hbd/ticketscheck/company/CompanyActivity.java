package com.hbd.ticketscheck.company;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.company.adapter.CompanyAdapter;
import com.hbd.ticketscheck.company.bean.CompanyBean;
import com.hbd.ticketscheck.company.presenter.CompanyPresenter;
import com.hbd.ticketscheck.company.view.CompanyView;
import com.hbd.ticketscheck.store.StoreUpdateDialog;
import com.hbd.ticketscheck.store.adapter.StoreAdapter;
import com.hbd.ticketscheck.view.LoadingDialog;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanyActivity extends BaseMvpActivity<CompanyPresenter> implements CompanyView {

    private ImageView back;
    private SmartRefreshLayout smartRefreshLayout;
    private RecyclerView recyclerView;
    private Button btnAdd;

    private CompanyAdapter adapter;
    private List<CompanyBean.ProductListBean> list;

    private int pageNo = 1;
    private int pageSize = 10;
    private int total;
    private LoadingDialog dialog;

    @Override
    protected void initView() {
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        smartRefreshLayout = findViewById(R.id.smartrefresh);
        recyclerView = findViewById(R.id.company_list);
        btnAdd = findViewById(R.id.btn_add);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(context));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(context));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {

                pageNo = 1;
                smartRefreshLayout.setEnableLoadMore(true);//启用加载
                initList();
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                initList();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CompanyUpdateDialog companyUpdateDialog = new CompanyUpdateDialog(context,activity);
                companyUpdateDialog.show();
            }
        });
        initData();
    }

    private void initData() {
        list = new ArrayList<>();
        adapter = new CompanyAdapter(list,context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        mPresenter.getMyTravelList(pageNo,pageSize);
        adapter.setOnItemClick(new StoreAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position) {
                CompanyUpdateDialog companyUpdateDialog = new CompanyUpdateDialog(context,activity);
                companyUpdateDialog.show();
                companyUpdateDialog.setDate(list.get(position));
            }
        });
    }

    private void initList() {

        mPresenter.getMyTravelList(pageNo,pageSize);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_company;
    }

    @Override
    protected CompanyPresenter createPresenter() {
        return new CompanyPresenter(this);
    }

    @Override
    public void getData(CompanyBean companyBean) {
        total = companyBean.getTotal();
        if(pageNo == 1){
            list.clear();
            smartRefreshLayout.finishRefresh();
        }else {
            smartRefreshLayout.finishLoadMore();
        }
        list.addAll(companyBean.getProductList());
        adapter.notifyDataSetChanged();
        pageNo++;
        if(list.size() == total){
            smartRefreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void updateSuccess() {
        pageNo = 1;
        smartRefreshLayout.setEnableLoadMore(true);//启用加载
        initList();
    }

    @Override
    public void updateError(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this,"玩命加载中...");
        dialog.show();
    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }

    public void saveData(int id, String travelNo, String travelName, String travelMobile, String contectName){

        Map<String,Object> params = new HashMap<>();
        if(id != -1){
            params.put("id",id);
        }

        params.put("travelNo",travelNo);
        params.put("travelMobile",travelMobile);
        params.put("travelName",travelName);
        params.put("contectName",contectName);

        mPresenter.myTravelupdateOrAdd(params);

    }
}
