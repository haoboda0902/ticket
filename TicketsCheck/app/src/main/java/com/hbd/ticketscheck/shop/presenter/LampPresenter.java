package com.hbd.ticketscheck.shop.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.shop.bean.LampListBean;
import com.hbd.ticketscheck.shop.bean.MetrailBean;
import com.hbd.ticketscheck.shop.iview.LampView;
import com.hbd.ticketscheck.shop.iview.MaterialView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class LampPresenter extends MyBasePresenter<LampView> {

    public LampPresenter(LampView materialView) {
        attachView(materialView);
    }

    public void getGoodsListByApp(int pageNo,int pageSize,String showData){
        Map<String,Object> params = new HashMap<>();
        params.put("pageNo",pageNo);
        params.put("pageSize",pageSize);
        params.put("showData",showData);
        subscribe(apiServer.getGoodsListByApp(params), new BaseObserver<BaseModel<LampListBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<LampListBean> baseModel) {
                LogUtils.d(baseModel.toString());
                LampListBean lampListBean = baseModel.getBody();

                if(lampListBean != null) {

                    iView.getList(lampListBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
            }
        });
    }

    public void getGoodsDateList() {
        subscribe(apiServer.getGoodsDateList(), new BaseObserver<BaseModel<String[]>>(iView) {

            @Override
            public void onSuccess(BaseModel<String[]> baseModel) {
                LogUtils.d(baseModel.toString());
                List<String> lampListBean = Arrays.asList(baseModel.getBody());

                if(lampListBean != null) {

                    iView.getDate(lampListBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
            }
        });
    }

    public void updateOrAdd(Map<String,Object> param) {

        iView.showLoading();
        subscribe(apiServer.lampupdateOrAdd(param), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                Object storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.upDateSuccess();
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.hideLoading();
                iView.upDateError(msg);
            }
        });
    }
}
