package com.hbd.ticketscheck.result.iview;

import com.hbd.network.baseview.IBaseView;

public interface IResultView extends IBaseView {

    void success();

    void error(String msg);

}
