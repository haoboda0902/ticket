package com.hbd.ticketscheck.result.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.login.bean.UserBean;
import com.hbd.ticketscheck.result.iview.IResultView;
import com.tencent.mmkv.MMKV;

import java.util.HashMap;
import java.util.Map;

public class ResultPresenter extends MyBasePresenter<IResultView> {

    public ResultPresenter(IResultView iResultView) {
        attachView(iResultView);
    }

    public void check(String token,String userId,String code){
        Map<String,String> params = new HashMap<>();
        params.put("token",token);
        params.put("userid",userId);
        params.put("orderNo",code);
        iView.showLoading();
        subscribe(apiServer.checkOrder(params), new BaseObserver<BaseModel<UserBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<UserBean> baseModel) {
                iView.success();
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.error(msg);
            }
        });
    }
}
