package com.hbd.ticketscheck.shop;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.LampListBean;
import com.hbd.ticketscheck.shop.bean.UpLoadBean;
import com.hbd.ticketscheck.shop.iview.LampUpdateView;
import com.hbd.ticketscheck.shop.presenter.LampUpdatePresenter;
import com.hbd.ticketscheck.utils.GlideEngine;
import com.hbd.ticketscheck.utils.GlideUtils;
import com.hbd.ticketscheck.view.LoadingDialog;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LampUpdateActivity extends BaseMvpActivity<LampUpdatePresenter> implements LampUpdateView {

    private ImageView back;
    private TextView shop_lamp_day,time,shop_lamp_over_time,order_commit;
    private ImageView lamp_img;
    private RadioGroup lamp_radiogroup;
    private EditText etPrice,lamp_name;

    private LampListBean.GoodsListBean goodsListBean;
    private String pic;
    private int lightNum = 1;
    private String showTime;
    private String overTime;
    private String actionTime;
    private double price;
    private String name;
    private LoadingDialog dialog;

    @Override
    protected void initView() {
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        time = findViewById(R.id.shop_lamp_time);
        shop_lamp_over_time = findViewById(R.id.shop_lamp_over_time);
        lamp_img = findViewById(R.id.lamp_img);
        lamp_radiogroup = findViewById(R.id.lamp_radiogroup);
        shop_lamp_day = findViewById(R.id.shop_lamp_day);
        etPrice = findViewById(R.id.shop_lamp_price);
        lamp_name = findViewById(R.id.lamp_name);
        order_commit = findViewById(R.id.order_commit);
        lamp_radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()){
                    case R.id.lamp_radiogroup_one:
                        lightNum = 1;
                        break;
                    case R.id.lamp_radiogroup_two:
                        lightNum = 2;
                        break;
                }
            }
        });
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTime();
            }
        });
        shop_lamp_over_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEndTime();
            }
        });
        lamp_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PictureSelector.create(LampUpdateActivity.this)
                        .openGallery(PictureMimeType.ofImage())
                        .loadImageEngine(GlideEngine.createGlideEngine())
                        .selectionMode(PictureConfig.SINGLE)
                        .imageFormat(PictureMimeType.PNG)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
            }
        });
        order_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commit();
            }
        });
        getIntentData();
    }

    private void commit() {

        if(TextUtils.isEmpty(showTime)){
            Toast.makeText(context,"请选择演出时间",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(overTime)){
            Toast.makeText(context,"请选择结束时间",Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(pic)){
            Toast.makeText(context,"请选择图片",Toast.LENGTH_SHORT).show();
            return;
        }

         name = lamp_name.getText().toString().trim();

        if(TextUtils.isEmpty(name)){
            Toast.makeText(context,"请输入名称",Toast.LENGTH_SHORT).show();
            return;
        }
        price = Double.parseDouble(etPrice.getText().toString().trim());
        if(price == 0){
            Toast.makeText(context,"请输入价格",Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String,Object> params = new HashMap<>();
        params.put("goodsName",name);
        params.put("lastSaldateStr",goodsListBean.getShowDate() + " "+overTime +":00");
        params.put("lightNum",lightNum);
        params.put("pic",pic);
        params.put("showTime",showTime);
        params.put("showDateStr",goodsListBean.getShowDate());
        params.put("oldPrice",price);
        params.put("goodsPrice",price);
        params.put("goodsType",1);
        params.put("totalNum",1000);

        if(goodsListBean.getId() != -1){
            params.put("id",goodsListBean.getId());
        }
        Log.e("param",params.toString());
        mPresenter.updateOrAdd(params);

    }

    private void getIntentData() {

        goodsListBean = (LampListBean.GoodsListBean) getIntent().getSerializableExtra("data");
        if(goodsListBean != null){
            actionTime = goodsListBean.getShowDate() == null ? goodsListBean.getLastSaldate() : goodsListBean.getShowDate();
            showTime = goodsListBean.getShowTime();
            overTime =  goodsListBean.getLastSaldate();
            pic = goodsListBean.getPic();
            price = goodsListBean.getGoodsPrice();
            name = goodsListBean.getGoodsName();
            shop_lamp_day.setText("时间： "+(goodsListBean.getShowDate() == null ? goodsListBean.getLastSaldate() : goodsListBean.getShowDate()));
            time.setText("活动时间："+(goodsListBean.getShowTime() == null ? "" : goodsListBean.getShowTime()));
            shop_lamp_over_time.setText("结束购票时间：" + (goodsListBean.getLastSaldate()  == null ? "" : goodsListBean.getLastSaldate()));
            GlideUtils.loadUrlImage(context,goodsListBean.getPic(),lamp_img);
            if(goodsListBean.getLightNum() == 1){
                lamp_radiogroup.check(R.id.lamp_radiogroup_one);
            }else{
                lamp_radiogroup.check(R.id.lamp_radiogroup_two);
            }
            etPrice.setText(goodsListBean.getGoodsPrice()+"");
            lamp_name.setText(goodsListBean.getGoodsName());
        }
    }

    private void showEndTime() {

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_time, null);
        final TimePicker endTime =  view.findViewById(R.id.et);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("选择时间");
        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String et = "" + (endTime.getHour() == 0 ? 12 : endTime.getHour()) +":"+(endTime.getMinute() < 10 ? "0" + endTime.getMinute() :endTime.getMinute());
                shop_lamp_over_time.setText("结束购票时间：  "+et);
                overTime =  goodsListBean.getLastSaldate() + " "+et;
                Log.e("时间", "~~~~~~"+et);
            }
        });
        builder.setNegativeButton("取消", null);
        AlertDialog dialog = builder.create();
        dialog.show();
        //自动弹出键盘问题解决
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void showTime() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_two_time, null);
        final TimePicker startTime = view.findViewById(R.id.st);
        final TimePicker endTime =  view.findViewById(R.id.et);
//        startTime.(startTime.getYear(), startTime.getMonth(), 01);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("选择时间");
        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(startTime.getHour() >= endTime.getHour() && startTime.getMinute() >= endTime.getMinute()){
                    Toast.makeText(context,"选择时间不合理",Toast.LENGTH_SHORT).show();
                    return;
                }
                String st = "" + (startTime.getHour() == 0 ? 12 : startTime.getHour()) +":"+(startTime.getMinute() < 10 ? "0" + startTime.getMinute() :startTime.getMinute());
                String et = "" + (endTime.getHour() == 0 ? 12 : endTime.getHour()) +":"+(endTime.getMinute() < 10 ? "0" + endTime.getMinute() :endTime.getMinute());
                time.setText("活动时间：  "+st+"-"+et);
                showTime = st+"-"+et;
                Log.e("时间",st + "~~~~~~"+et);
            }
        });
        builder.setNegativeButton("取消", null);
        AlertDialog dialog = builder.create();
        dialog.show();
        //自动弹出键盘问题解决
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activty_lamp_update;
    }

    @Override
    protected LampUpdatePresenter createPresenter() {
        return new LampUpdatePresenter(this);
    }

    @Override
    public void upDateSuccess() {
            activity.finish();
    }

    @Override
    public void upLoadSuccess(UpLoadBean upLoadBean) {

            pic = upLoadBean.getUrl();
            GlideUtils.loadUrlImage(context,pic,lamp_img);

    }

    @Override
    public void upDateError(String msg) {
        Toast.makeText(context,"更新失败",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this,"玩命加载中...");
        dialog.show();
    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 结果回调
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    if(selectList != null && selectList.size() > 0){
                        String path = selectList.get(0).getRealPath();
                        upLoadPic(path);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void upLoadPic(String path) {

        mPresenter.upLoad(path);

    }
}
