package com.hbd.ticketscheck.orderupdate.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.orderupdate.bean.OrderItems;
import com.hbd.ticketscheck.orderupdate.ivew.OrderUpdateView;

import java.util.HashMap;
import java.util.Map;

public class OrderUpdatePresenter extends MyBasePresenter<OrderUpdateView> {

    public OrderUpdatePresenter(OrderUpdateView iWelcomeView) {
        attachView(iWelcomeView);
    }

    public void save(String orderNo,String postageNo){

            Map<String,String> params = new HashMap<>();
            params.put("orderNo", orderNo);
            params.put("postageNo", postageNo);
            iView.showLoading();
            subscribe(apiServer.opeOrderForAdmin(params), new BaseObserver<BaseModel<Object>>(iView) {

                @Override
                public void onSuccess(BaseModel<Object> baseModel) {
                    LogUtils.d(baseModel.toString());
                    Object orderBean = baseModel.getBody();
                    if(orderBean != null) {
                        iView.saveSuccess();

                    }else{
                        onError(baseModel.getReturnTip());
                    }
                }

                @Override
                public void onError(String msg) {
                    LogUtils.d(msg.toString());
                    iView.saveError(msg);
                }
            });


    }

    public void getProductOrderItemForApp(String orderNo){

        Map<String,String> params = new HashMap<>();
        params.put("orderNo", orderNo);
        iView.showLoading();
        subscribe(apiServer.getProductOrderItemForApp(params), new BaseObserver<BaseModel<OrderItems>>(iView) {

            @Override
            public void onSuccess(BaseModel<OrderItems> baseModel) {
                LogUtils.d(baseModel.toString());
                OrderItems orderBean = baseModel.getBody();
                if(orderBean != null) {
                    iView.getOrder(orderBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());

                iView.getOrderError(msg);
            }
        });


    }
}
