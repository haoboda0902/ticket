package com.hbd.ticketscheck.api;

import com.hbd.network.apimanager.ApiManager;
import com.hbd.network.basepresenter.BasePresenter;
import com.hbd.network.baseview.IBaseView;

public class MyBasePresenter<V extends IBaseView> extends BasePresenter<V>{


    public ApiServer apiServer;

    @Override
    public void attachView(V iBaseView) {
        iView = iBaseView;
        apiServer = ApiManager.getApi().initRetrofit().create(ApiServer.class);
    }
}
