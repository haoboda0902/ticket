package com.hbd.ticketscheck.store.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shop.bean.StoreBean;
import com.hbd.ticketscheck.shoptype.iview.ShopTypeView;
import com.hbd.ticketscheck.store.iview.StoreView;

import java.util.Map;

public class StorePresenter extends MyBasePresenter<StoreView> {

    public StorePresenter(StoreView materialView) {
        attachView(materialView);
    }


    public void getMyStoreList() {
        subscribe(apiServer.getMyStoreList(), new BaseObserver<BaseModel<StoreBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<StoreBean> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                StoreBean storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.getStorreSuccess(storeBean);
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.getStoreError(msg);
                LogUtils.d(msg.toString());
            }
        });
    }
    public void myStoreupdateOrAdd(Map<String,Object> params) {
        iView.showLoading();
        subscribe(apiServer.myStoreupdateOrAdd(params), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                Object storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.updateSuccess();
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.hideLoading();
                iView.updateError(msg);
                LogUtils.d(msg.toString());
            }
        });
    }
}
