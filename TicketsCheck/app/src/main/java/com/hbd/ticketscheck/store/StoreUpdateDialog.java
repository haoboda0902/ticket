package com.hbd.ticketscheck.store;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.StoreBean;

public class StoreUpdateDialog  extends Dialog {

    private Context context;
    private Activity activity;
    private EditText store_name,manager_name,manager_account,manager_pwd,manager_phone;
    private Button save;

    private int id = -1;

    public StoreUpdateDialog(Context context,Activity activity) {
        super(context);
        this.context = context;
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_store_update);
        store_name = findViewById(R.id.store_name);
        manager_name = findViewById(R.id.manager_name);
        manager_account = findViewById(R.id.manager_account);
        manager_pwd = findViewById(R.id.manager_pwd);
        manager_phone = findViewById(R.id.manager_phone);
        save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });
    }

    private void saveData() {

        if( !(activity instanceof StoreActivity)){
            return;
        }

        if(store_name.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入仓库名称",Toast.LENGTH_SHORT).show();
            return;
        }
        if(manager_name.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入管理员名称",Toast.LENGTH_SHORT).show();
            return;
        }
        if(manager_account.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入管理员账号",Toast.LENGTH_SHORT).show();
            return;
        }
        if(manager_pwd.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入管理员密码",Toast.LENGTH_SHORT).show();
            return;
        }
        if(manager_phone.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入管理员电话",Toast.LENGTH_SHORT).show();
            return;
        }

        ((StoreActivity) activity).saveStore(store_name.getText().toString().trim(),manager_name.getText().toString().trim(),manager_account.getText().toString().trim(),
                manager_pwd.getText().toString().trim(),manager_phone.getText().toString().trim(),id);
        this.dismiss();
    }

    public void setData(StoreBean.StoreListBean storeListBean){
        id = storeListBean.getId();
        store_name.setText(storeListBean.getStoreName());
        manager_name.setText(storeListBean.getManagerName());
        manager_account.setText(storeListBean.getManagerAccount());
        manager_pwd.setText(storeListBean.getManagerPassswd());
        manager_phone.setText(storeListBean.getManagerMobile());

    }
}
