package com.hbd.ticketscheck.company;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.company.bean.CompanyBean;
import com.hbd.ticketscheck.store.StoreActivity;
import com.hbd.ticketscheck.utils.GlideUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

public class CompanyUpdateDialog extends Dialog {

    private EditText travel_id,travel_name,travel_phone,manager_name;
    private ImageView erqode;
    private Button btnAdd;
    private LinearLayout ly_erqode;

    private Context context;
    private Activity activity;

    private int id = -1;

    public CompanyUpdateDialog(Context context,Activity activity) {
        super(context);
        this.context = context;
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_company_update);

        travel_id = findViewById(R.id.travel_id);
        travel_name = findViewById(R.id.travel_name);
        travel_phone = findViewById(R.id.travel_phone);
        manager_name = findViewById(R.id.manager_name);
        erqode = findViewById(R.id.travel_erqode);
        btnAdd = findViewById(R.id.save);
        ly_erqode = findViewById(R.id.ly_erqode);
        ly_erqode.setVisibility(View.GONE);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });

    }
    private void saveData() {

        if( !(activity instanceof CompanyActivity)){
            return;
        }

        if(travel_id.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入旅行社ID",Toast.LENGTH_SHORT).show();
            return;
        }
        if(travel_name.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入旅行社名称",Toast.LENGTH_SHORT).show();
            return;
        }
        if(travel_phone.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入旅行社电话",Toast.LENGTH_SHORT).show();
            return;
        }
        if(manager_name.getText().toString().trim().length() < 1){
            Toast.makeText(context,"请输入负责人名称",Toast.LENGTH_SHORT).show();
            return;
        }
        ((CompanyActivity) activity).saveData(id,travel_id.getText().toString().trim(),travel_name.getText().toString().trim(),
                travel_phone.getText().toString().trim(),manager_name.getText().toString().trim());
        this.dismiss();
    }

    public void setDate(CompanyBean.ProductListBean productListBean){
        id = productListBean.getId();
        travel_id.setText(productListBean.getTravelNo()+"");
        travel_name.setText(productListBean.getTravelName());
        travel_phone.setText(productListBean.getTravelMobile());
        manager_name.setText(productListBean.getContectName());
        ly_erqode.setVisibility(View.VISIBLE);
        GlideUtils.loadUrlImage(context,productListBean.getDesc(),erqode);
        erqode.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Glide.with(context)
                                          .asBitmap()
                                             .load(productListBean.getDesc())
                                             .into(new SimpleTarget<Bitmap>() {
                                 @Override
                                  public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                            saveImage(resource);
                                                         }
                             });

                return false;
            }
        });
    }

    private void saveImage(Bitmap image) {
                String saveImagePath = null;
                Random random = new Random();
                String imageFileName = "JPEG_" + "down" + random.nextInt(10) + ".jpg";
                File storageDir = new File(Environment.getExternalStorageDirectory() + "/check/"+imageFileName);

                 boolean success = true;
                if(!storageDir.exists()){
                    try {
                        success = storageDir.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                 if(success){
//                        File imageFile = new File(storageDir, imageFileName);
                         saveImagePath = storageDir.getAbsolutePath();
                         try {
                                 OutputStream fout = new FileOutputStream(storageDir);
                                 image.compress(Bitmap.CompressFormat.JPEG, 100, fout);
                                 fout.close();
                             } catch (Exception e) {
                                 e.printStackTrace();
                             }

                         // Add the image to the system gallery
                         galleryAddPic(saveImagePath);
                         Toast.makeText(context, "IMAGE SAVED", Toast.LENGTH_LONG).show();
                     }
         //        return saveImagePath;
             }

    private void galleryAddPic(String imagePath) {
                 Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                 File f = new File(imagePath);
                 Uri contentUri = Uri.fromFile(f);
                 mediaScanIntent.setData(contentUri);
                 context.sendBroadcast(mediaScanIntent);
             }
}
