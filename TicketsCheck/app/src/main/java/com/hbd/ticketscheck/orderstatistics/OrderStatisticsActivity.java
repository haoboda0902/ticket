package com.hbd.ticketscheck.orderstatistics;

import android.app.DatePickerDialog;
import android.os.Build;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.orderstatistics.adapter.SumAdapter;
import com.hbd.ticketscheck.orderstatistics.bean.SumOrder;
import com.hbd.ticketscheck.orderstatistics.presenter.StatisticsPresenter;
import com.hbd.ticketscheck.orderstatistics.view.StatisticsView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderStatisticsActivity extends BaseMvpActivity<StatisticsPresenter> implements StatisticsView {

    private ImageView back;
    private TabLayout tabLayout;
    private LinearLayout lyStartTime;
    private TextView tvStartTime;
    private LinearLayout lyEndTime;
    private TextView tvEndTime;
    private TextView search;
    private RadioGroup radioGroup;
    private RecyclerView recyclerView;
    private TextView price;

    private String startDateStr,endDateStr;
    private int queryGrade = 1; //1:年度 2：月度 3：周度 4： 日度 5：具体日期
    private int goodsType = 1; //1:个人 2：团购 3：网红

    private SumAdapter adapter;
    private List<SumOrder.GoodsListBean> listBeans;;

    @Override
    protected void initView() {
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        tabLayout = findViewById(R.id.tablayout);
        lyStartTime = findViewById(R.id.ly_start_time);
        tvStartTime = findViewById(R.id.tv_start_time);
        lyEndTime = findViewById(R.id.ly_end_time);
        tvEndTime = findViewById(R.id.tv_end_time);
        search = findViewById(R.id.tv_search);
        radioGroup = findViewById(R.id.rg_group);
        recyclerView = findViewById(R.id.rv_list);
        price = findViewById(R.id.tv_price);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                goodsType = i+1;
            }
        });

        lyStartTime.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(context);
                datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        startDateStr = i +"-"+(i1 + 1)+"-"+i2;
                        tvStartTime.setText(startDateStr);
                    }
                });
                datePickerDialog.show();
            }
        });
        lyEndTime.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(context);
                datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        endDateStr = i +"-"+(i1 + 1)+"-"+i2;
                        tvEndTime.setText(endDateStr);
                    }
                });
                datePickerDialog.show();
            }
        });


        tabLayout.addTab(tabLayout.newTab().setText("年度统计"));
        tabLayout.addTab(tabLayout.newTab().setText("本月统计"));
        tabLayout.addTab(tabLayout.newTab().setText("本周统计"));
        tabLayout.addTab(tabLayout.newTab().setText("本日统计"));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
               queryGrade = tab.getPosition()+1;

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                search();
            }
        });
    }

    private void search() {

        if(startDateStr == null){
            Toast.makeText(context,"请选择开始时间",Toast.LENGTH_SHORT).show();
            return;
        }

        if(endDateStr == null){
            Toast.makeText(context,"请选择结束时间",Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String,Object> params = new HashMap<>();
        params.put("startDateStr",startDateStr);
        params.put("endDateStr",endDateStr);
        params.put("queryGrade",queryGrade);
        params.put("goodsType",goodsType);
        mPresenter.search(params);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order_statistics;
    }

    @Override
    protected StatisticsPresenter createPresenter() {
        return new StatisticsPresenter(this);
    }


    @Override
    public void getDataSuccess(SumOrder sunOrder) {
        listBeans = new ArrayList<>();
        listBeans.addAll(sunOrder.getGoodsList());
        adapter = new SumAdapter(listBeans,context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        price.setText("总价："+sunOrder.getTotalMoney());

    }

    @Override
    public void getdataError(String msg) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
