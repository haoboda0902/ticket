package com.hbd.ticketscheck.shop;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.hbd.network.BaseMvpActivity;
import com.hbd.network.basepresenter.BasePresenter;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.fragment.LampFragment;
import com.hbd.ticketscheck.shop.fragment.MaterialFragment;

import java.util.ArrayList;
import java.util.List;

public class ShopUpdateActivity extends BaseMvpActivity {

    private ImageView back;
    private RadioGroup shopType;
    private ViewPager viewPager;
    private MaterialFragment materialFragment;
    private LampFragment lampFragment;
    private List<Fragment> list;

    @Override
    protected void initView() {
        shopType = findViewById(R.id.shop_type);
        viewPager = findViewById(R.id.shop_viewpager);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        materialFragment = new MaterialFragment();
        lampFragment = new LampFragment();
        list = new ArrayList<>();
        list.add(materialFragment);
        list.add(lampFragment);
        viewPager.setAdapter(new ShopPagerAdapter(getSupportFragmentManager(),FragmentPagerAdapter.BEHAVIOR_SET_USER_VISIBLE_HINT));
        shopType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if(radioGroup.getCheckedRadioButtonId() == R.id.shop_material){
                    viewPager.setCurrentItem(0);
                }else {
                    viewPager.setCurrentItem(1);
                }

            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_shopupdate;
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    public class ShopPagerAdapter  extends FragmentPagerAdapter{


        public ShopPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }
    }
}
