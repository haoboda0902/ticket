package com.hbd.ticketscheck.shop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.MetrailBean;

import java.util.List;

public class MeterailAdapter extends RecyclerView.Adapter<MeterailAdapter.ViewHolder> {

    private List<MetrailBean.ProductListBean> list;
    private Context context;

    private OnItemClickLisenter onItemClickLisenter;

    public void setOnItemClickLisenter(OnItemClickLisenter onItemClickLisenter){
        this.onItemClickLisenter = onItemClickLisenter;
    }

    public MeterailAdapter(List<MetrailBean.ProductListBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MeterailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_shop_material,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeterailAdapter.ViewHolder holder, final int position) {

        holder.name.setText(list.get(position).getProductName());
        holder.price.setText(list.get(position).getProductPrice()+"元");
        Glide.with(context).load(list.get(position).getPic()).into(holder.imageView);
        holder.upDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickLisenter.upDown(position);
            }
        });

        if(list.get(position).getIsPut() == 0){
            holder.upDown.setText("上架");
        }else{
            holder.upDown.setText("下架");
        }
        holder.upDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickLisenter.update(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private TextView name,price;
        private Button upDown,upDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.shop_material_img);
            name = itemView.findViewById(R.id.shop_material_name);
            price = itemView.findViewById(R.id.shop_material_price);
            upDown = itemView.findViewById(R.id.shop_material_up);
            upDate = itemView.findViewById(R.id.shop_material_update);

        }
    }

    public interface OnItemClickLisenter{

        void upDown(int position);
        void update(int position);
    }
}
