package com.hbd.ticketscheck.welcome.iview;

import com.hbd.network.baseview.IBaseView;

public interface IWelcomeView extends IBaseView {

    void loginSuccess();

    void loginError(String msg);

}
