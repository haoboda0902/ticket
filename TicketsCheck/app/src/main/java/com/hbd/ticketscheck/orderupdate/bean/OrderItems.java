package com.hbd.ticketscheck.orderupdate.bean;

import java.io.Serializable;
import java.util.List;

public class OrderItems implements Serializable {


    private List<OrderItemsBean> orderItems;

    public List<OrderItemsBean> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItemsBean> orderItems) {
        this.orderItems = orderItems;
    }

    public static class OrderItemsBean {
        /**
         * checkDate :
         * commonPostage : 12.0
         * createDate : 2020-11-14 11:49:56
         * createUser : 1
         * desPic : http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg
         * desc :
         * id : 258
         * orderNo : 10RP202011141149555610090821
         * orderNum : 6
         * pic : http://scenicspotstatic.bjyidongtianqi.com/scenicspot/2020-11-07/e96892b4ab654863af6b29a81e039be7.jpg
         * productId : 1
         * productInfo : 21:30  22:30
         * productName : 21:30  22:30
         * productPrice : 12.0
         * productType : 0
         * productWeight : 1.0
         * receiverMobile : 132213124
         * receiverName : test
         * specialPostage : 16.0
         * storeId : 1
         * updateDate :
         * updateUser :
         */

        private String checkDate;
        private double commonPostage;
        private String createDate;
        private String createUser;
        private String desPic;
        private String desc;
        private int id;
        private String orderNo;
        private int orderNum;
        private String pic;
        private int productId;
        private String productInfo;
        private String productName;
        private double productPrice;
        private int productType;
        private double productWeight;
        private String receiverMobile;
        private String receiverName;
        private double specialPostage;
        private int storeId;
        private String updateDate;
        private String updateUser;

        public String getCheckDate() {
            return checkDate;
        }

        public void setCheckDate(String checkDate) {
            this.checkDate = checkDate;
        }

        public double getCommonPostage() {
            return commonPostage;
        }

        public void setCommonPostage(double commonPostage) {
            this.commonPostage = commonPostage;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getDesPic() {
            return desPic;
        }

        public void setDesPic(String desPic) {
            this.desPic = desPic;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public int getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(int orderNum) {
            this.orderNum = orderNum;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public String getProductInfo() {
            return productInfo;
        }

        public void setProductInfo(String productInfo) {
            this.productInfo = productInfo;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public double getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(double productPrice) {
            this.productPrice = productPrice;
        }

        public int getProductType() {
            return productType;
        }

        public void setProductType(int productType) {
            this.productType = productType;
        }

        public double getProductWeight() {
            return productWeight;
        }

        public void setProductWeight(double productWeight) {
            this.productWeight = productWeight;
        }

        public String getReceiverMobile() {
            return receiverMobile;
        }

        public void setReceiverMobile(String receiverMobile) {
            this.receiverMobile = receiverMobile;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public double getSpecialPostage() {
            return specialPostage;
        }

        public void setSpecialPostage(double specialPostage) {
            this.specialPostage = specialPostage;
        }

        public int getStoreId() {
            return storeId;
        }

        public void setStoreId(int storeId) {
            this.storeId = storeId;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }
    }
}
