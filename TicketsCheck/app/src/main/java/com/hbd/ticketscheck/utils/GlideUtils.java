package com.hbd.ticketscheck.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class GlideUtils {

    public static void loadUrlImage(Context context, String url, ImageView imageView){

//        if (url.startsWith("http://")) {
//            url = url.replace("http://", "https://");
//        }

        Glide.with(context).load(url).into(imageView);
    }

}
