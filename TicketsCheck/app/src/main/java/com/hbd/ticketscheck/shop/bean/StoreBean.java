package com.hbd.ticketscheck.shop.bean;

import java.io.Serializable;
import java.util.List;

public class StoreBean implements Serializable {


    private List<StoreListBean> storeList;

    public List<StoreListBean> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<StoreListBean> storeList) {
        this.storeList = storeList;
    }

    public static class StoreListBean {
        /**
         * createDate : {"date":1,"day":0,"hours":20,"minutes":2,"month":10,"seconds":4,"time":1604232124000,"timezoneOffset":-480,"year":120}
         * createUser : 123
         * desc :
         * id : 1
         * managerAccount : testAccout
         * managerMobile : 12312412
         * managerName : test
         * managerPassswd : 123123
         * storeName : c1
         * updateDate : {"date":1,"day":0,"hours":20,"minutes":2,"month":10,"seconds":4,"time":1604232124000,"timezoneOffset":-480,"year":120}
         * updateUser :
         */

        private String createDate;
        private String createUser;
        private String desc;
        private int id;
        private String managerAccount;
        private String managerMobile;
        private String managerName;
        private String managerPassswd;
        private String storeName;
        private String updateDate;
        private String updateUser;

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getManagerAccount() {
            return managerAccount;
        }

        public void setManagerAccount(String managerAccount) {
            this.managerAccount = managerAccount;
        }

        public String getManagerMobile() {
            return managerMobile;
        }

        public void setManagerMobile(String managerMobile) {
            this.managerMobile = managerMobile;
        }

        public String getManagerName() {
            return managerName;
        }

        public void setManagerName(String managerName) {
            this.managerName = managerName;
        }

        public String getManagerPassswd() {
            return managerPassswd;
        }

        public void setManagerPassswd(String managerPassswd) {
            this.managerPassswd = managerPassswd;
        }

        public String getStoreName() {
            return storeName;
        }

        public void setStoreName(String storeName) {
            this.storeName = storeName;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }

        public static class CreateDateBean {
            /**
             * date : 1
             * day : 0
             * hours : 20
             * minutes : 2
             * month : 10
             * seconds : 4
             * time : 1604232124000
             * timezoneOffset : -480
             * year : 120
             */

            private int date;
            private int day;
            private int hours;
            private int minutes;
            private int month;
            private int seconds;
            private long time;
            private int timezoneOffset;
            private int year;

            public int getDate() {
                return date;
            }

            public void setDate(int date) {
                this.date = date;
            }

            public int getDay() {
                return day;
            }

            public void setDay(int day) {
                this.day = day;
            }

            public int getHours() {
                return hours;
            }

            public void setHours(int hours) {
                this.hours = hours;
            }

            public int getMinutes() {
                return minutes;
            }

            public void setMinutes(int minutes) {
                this.minutes = minutes;
            }

            public int getMonth() {
                return month;
            }

            public void setMonth(int month) {
                this.month = month;
            }

            public int getSeconds() {
                return seconds;
            }

            public void setSeconds(int seconds) {
                this.seconds = seconds;
            }

            public long getTime() {
                return time;
            }

            public void setTime(long time) {
                this.time = time;
            }

            public int getTimezoneOffset() {
                return timezoneOffset;
            }

            public void setTimezoneOffset(int timezoneOffset) {
                this.timezoneOffset = timezoneOffset;
            }

            public int getYear() {
                return year;
            }

            public void setYear(int year) {
                this.year = year;
            }
        }

        public static class UpdateDateBean {
            /**
             * date : 1
             * day : 0
             * hours : 20
             * minutes : 2
             * month : 10
             * seconds : 4
             * time : 1604232124000
             * timezoneOffset : -480
             * year : 120
             */

            private int date;
            private int day;
            private int hours;
            private int minutes;
            private int month;
            private int seconds;
            private long time;
            private int timezoneOffset;
            private int year;

            public int getDate() {
                return date;
            }

            public void setDate(int date) {
                this.date = date;
            }

            public int getDay() {
                return day;
            }

            public void setDay(int day) {
                this.day = day;
            }

            public int getHours() {
                return hours;
            }

            public void setHours(int hours) {
                this.hours = hours;
            }

            public int getMinutes() {
                return minutes;
            }

            public void setMinutes(int minutes) {
                this.minutes = minutes;
            }

            public int getMonth() {
                return month;
            }

            public void setMonth(int month) {
                this.month = month;
            }

            public int getSeconds() {
                return seconds;
            }

            public void setSeconds(int seconds) {
                this.seconds = seconds;
            }

            public long getTime() {
                return time;
            }

            public void setTime(long time) {
                this.time = time;
            }

            public int getTimezoneOffset() {
                return timezoneOffset;
            }

            public void setTimezoneOffset(int timezoneOffset) {
                this.timezoneOffset = timezoneOffset;
            }

            public int getYear() {
                return year;
            }

            public void setYear(int year) {
                this.year = year;
            }
        }
    }
}
