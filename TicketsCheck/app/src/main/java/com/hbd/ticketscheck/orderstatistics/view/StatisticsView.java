package com.hbd.ticketscheck.orderstatistics.view;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.orderstatistics.bean.SumOrder;

public interface StatisticsView extends IBaseView {

    void getDataSuccess(SumOrder sunOrder);

    void getdataError(String msg);

}
