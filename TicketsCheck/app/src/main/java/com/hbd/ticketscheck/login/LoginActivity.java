package com.hbd.ticketscheck.login;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gyf.immersionbar.ImmersionBar;
import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.login.iview.LoginView;
import com.hbd.ticketscheck.login.presenter.LoginPresenter;
import com.hbd.ticketscheck.main.MainActivity;
import com.hbd.ticketscheck.main.NewMainActivity;
import com.hbd.ticketscheck.utils.MyCountDownTimer;
import com.hbd.ticketscheck.view.LoadingDialog;
import com.hbd.ticketscheck.welcome.WelcomeActivity;

public class LoginActivity extends BaseMvpActivity<LoginPresenter> implements LoginView{



    private EditText etMobile;
    private EditText etPwd;
    private EditText etCode;
    private TextView tvCode;
    private Button btnLogin;
    private MyCountDownTimer myCountDownTimer;
    private LoadingDialog dialog;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter(this);
    }

    @Override
    public void login() {
        Toast.makeText(this,"登录成功",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, NewMainActivity.class);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    @Override
    public void codeSuccess() {
        Toast.makeText(this,"发送成功",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void codeError(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginError(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this,"加载中...");

        dialog.show();

    }

    @Override
    public void hideLoading() {
        dialog.close();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void initView() {
//        int  statusBarHeight = ImmersionBar.getStatusBarHeight(this);
        etMobile = findViewById(R.id.et_login_mobile);
        etPwd = findViewById(R.id.et_login_pwd);
        etCode = findViewById(R.id.et_login_code);
        tvCode = findViewById(R.id.tv_login_code);
        btnLogin = findViewById(R.id.btn_login_commit);

        myCountDownTimer = new MyCountDownTimer(60000,1000,tvCode);

        tvCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myCountDownTimer.start();
                getVerCode();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commit();
            }
        });
    }

    private void getVerCode() {
        String moblie = etMobile.getText().toString().trim();
        String pwd = etPwd.getText().toString().trim();
        if(TextUtils.isEmpty(moblie)){
            Toast.makeText(this,"请输入用户名",Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(pwd)){
            Toast.makeText(this,"请输入密码",Toast.LENGTH_SHORT).show();
            return;
        }
        mPresenter.getVerCode(moblie,pwd);
    }

    private void commit() {
        String moblie = etMobile.getText().toString().trim();
        String pwd = etPwd.getText().toString().trim();
        String code = etCode.getText().toString().trim();

        if(TextUtils.isEmpty(moblie)){
            Toast.makeText(this,"请输入用户名",Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(pwd)){
            Toast.makeText(this,"请输入密码",Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(code)){
            Toast.makeText(this,"请输入验证码",Toast.LENGTH_SHORT).show();
            return;
        }

        mPresenter.login(moblie,pwd,code);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        timerStop();
    }

    private void timerStop() {
        if (myCountDownTimer != null) {
            myCountDownTimer.cancel();
            myCountDownTimer = null;
        }
    }


}
