package com.hbd.ticketscheck.shop.bean;

import java.io.Serializable;
import java.util.List;

public class MetrailBean implements Serializable {


    /**
     * total : 1
     * productList : [{"commonPostage":12,"createDate":"2020-11-01 19:22:33","createUser":"1","desPic":"123","desc":"","id":1,"isPut":1,"pic":"1312","productInfo":"123124","productName":"12","productPrice":12,"productType":1,"productWeight":1,"specialPostage":16,"storeId":1,"totalNum":1234,"updateDate":"","updateUser":""}]
     */

    private int total;
    private List<ProductListBean> productList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ProductListBean> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductListBean> productList) {
        this.productList = productList;
    }

    public static class ProductListBean implements Serializable{
        /**
         * commonPostage : 12.0
         * createDate : 2020-11-01 19:22:33
         * createUser : 1
         * desPic : 123
         * desc :
         * id : 1
         * isPut : 1
         * pic : 1312
         * productInfo : 123124
         * productName : 12
         * productPrice : 12.0
         * productType : 1
         * productWeight : 1.0
         * specialPostage : 16.0
         * storeId : 1
         * totalNum : 1234
         * updateDate :
         * updateUser :
         */

        private double commonPostage;
        private String createDate;
        private String createUser;
        private String desPic;
        private String desc;
        private int id;
        private int isPut;
        private String pic;
        private String productInfo;
        private String productName;
        private double productPrice;
        private int productType;
        private double productWeight;
        private double specialPostage;
        private int storeId;
        private int totalNum;
        private String updateDate;
        private String updateUser;

        public double getCommonPostage() {
            return commonPostage;
        }

        public void setCommonPostage(double commonPostage) {
            this.commonPostage = commonPostage;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getDesPic() {
            return desPic;
        }

        public void setDesPic(String desPic) {
            this.desPic = desPic;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsPut() {
            return isPut;
        }

        public void setIsPut(int isPut) {
            this.isPut = isPut;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getProductInfo() {
            return productInfo;
        }

        public void setProductInfo(String productInfo) {
            this.productInfo = productInfo;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public double getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(double productPrice) {
            this.productPrice = productPrice;
        }

        public int getProductType() {
            return productType;
        }

        public void setProductType(int productType) {
            this.productType = productType;
        }

        public double getProductWeight() {
            return productWeight;
        }

        public void setProductWeight(double productWeight) {
            this.productWeight = productWeight;
        }

        public double getSpecialPostage() {
            return specialPostage;
        }

        public void setSpecialPostage(double specialPostage) {
            this.specialPostage = specialPostage;
        }

        public int getStoreId() {
            return storeId;
        }

        public void setStoreId(int storeId) {
            this.storeId = storeId;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }
    }
}
