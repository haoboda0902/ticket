package com.hbd.ticketscheck.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.main.bean.ActionBean;

import java.util.List;

public class MainActionAdapter extends RecyclerView.Adapter<MainActionAdapter.ViewHolder> {

    private Context context;
    private List<ActionBean> list;
    private OnItemClickListener onItemClickListener;//声明一下这个接口
    public MainActionAdapter(Context context, List<ActionBean> list) {
        this.context = context;
        this.list = list;
    }
    //提供setter方法
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{//也可以不在这个activity或者是fragment中来声明接口，可以在项目中单独创建一个interface，就改成static就OK
        void onItemClick(int position);
    }
    @NonNull
    @Override
    public MainActionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_new_main_action,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainActionAdapter.ViewHolder holder, final int position) {
            holder.name.setText(list.get(position).getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(position);
                }
            });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tv_main_item_name);
        }
    }
}
