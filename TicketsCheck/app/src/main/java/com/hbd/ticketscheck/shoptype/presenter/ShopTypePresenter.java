package com.hbd.ticketscheck.shoptype.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shoptype.iview.ShopTypeView;

import java.util.Map;

public class ShopTypePresenter extends MyBasePresenter<ShopTypeView> {

    public ShopTypePresenter(ShopTypeView materialView) {
        attachView(materialView);
    }

   public void ShopTypePresenter(ShopTypeView shopTypeView){
       attachView(shopTypeView);
   }

    public void getProductTypeList() {
        subscribe(apiServer.getProductTypeList(), new BaseObserver<BaseModel<ShopTypeBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<ShopTypeBean> baseModel) {
                LogUtils.d(baseModel.toString());
                ShopTypeBean shopTypeBeans = baseModel.getBody();

                if(shopTypeBeans != null) {

                    iView.getShopSuccess(shopTypeBeans);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.hideLoading();
                LogUtils.d(msg.toString());
            }
        });
    }

    public void productTypeupdateOrAdd(Map<String,Object> param) {
        subscribe(apiServer.productTypeupdateOrAdd(param), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                LogUtils.d(baseModel.toString());
                Object shopTypeBeans = baseModel.getBody();

                if(shopTypeBeans != null) {

                    iView.getUpdateShopSuccess();
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.hideLoading();
                iView.getUpdateShopError(msg);
                LogUtils.d(msg.toString());
            }
        });
    }
    public void moveProductType(Map<String,Object> param) {
        iView.showLoading();
        subscribe(apiServer.moveProductType(param), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                iView.hideLoading();
                LogUtils.d(baseModel.toString());
                Object shopTypeBeans = baseModel.getBody();

                if(shopTypeBeans != null) {

                    iView.moveShopSuccess();
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.hideLoading();
                iView.moveShopError(msg);
                LogUtils.d(msg.toString());
            }
        });
    }
}
