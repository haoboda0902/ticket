package com.hbd.ticketscheck.shop.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpFragment;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.LampUpdateActivity;
import com.hbd.ticketscheck.shop.MaterialUpdateActivity;
import com.hbd.ticketscheck.shop.adapter.MeterailAdapter;
import com.hbd.ticketscheck.shop.bean.MetrailBean;
import com.hbd.ticketscheck.shop.iview.MaterialView;
import com.hbd.ticketscheck.shop.presenter.MaterialPresenter;
import com.hbd.ticketscheck.view.LoadingDialog;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaterialFragment extends BaseMvpFragment<MaterialPresenter> implements MaterialView {

    private SmartRefreshLayout smartRefreshLayout;
    private RecyclerView materialList;
    private Button materialAdd;

    private MeterailAdapter adapter;
    private List<MetrailBean.ProductListBean> list;
    private int pageNo = 1;
    private int pageSize = 10;
    private int total = 0;
    private int updatePosition = -1;
    private LoadingDialog dialog;


    @Override
    protected void initView(View view) {
        materialAdd = view.findViewById(R.id.material_add);
        materialList = view.findViewById(R.id.material_list);
        smartRefreshLayout = view.findViewById(R.id.smartrefresh);
        list = new ArrayList<>();
        adapter = new MeterailAdapter(list,this.getContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        materialList.setLayoutManager(linearLayoutManager);
        materialList.setAdapter(adapter);
        adapter.setOnItemClickLisenter(new MeterailAdapter.OnItemClickLisenter() {
            @Override
            public void upDown(int position) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("确定执行上下架操作吗").setPositiveButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        updatePosition = position;
                        Map<String,Object> param = new HashMap<>();
                        if(list.get(position).getIsPut() == 0){
                            param.put("isPut",1);
                        }else{
                            param.put("isPut",0);
                        }
                        param.put("id",list.get(position).getId());
                        mPresenter.updateOrAdd(param);
                    }
                }).show();

            }

            @Override
            public void update(int position) {
                Intent intent = new Intent(getActivity(), MaterialUpdateActivity.class);
                intent.putExtra("data", (Serializable) list.get(position));
                startActivity(intent);
            }
        });
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {

                pageNo = 1;
                smartRefreshLayout.setEnableLoadMore(true);//启用加载
                initData();
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                initData();
            }
        });
        materialAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MaterialUpdateActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {

        mPresenter.getDate(pageNo,pageSize);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frag_material;
    }

    @Override
    protected MaterialPresenter createPresenter() {
        return new MaterialPresenter(this);
    }

    @Override
    public void getDataSuccess(MetrailBean metrailBean) {

        total = metrailBean.getTotal();
        if(pageNo == 1){
            list.clear();
            smartRefreshLayout.finishRefresh();
        }else {
            smartRefreshLayout.finishLoadMore();
        }
        pageNo ++;
        list.addAll(metrailBean.getProductList());
        adapter.notifyDataSetChanged();
        if(list.size() == total){
            smartRefreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void upDateSuccess() {
        if(list.get(updatePosition).getIsPut() == 0){
            list.get(updatePosition).setIsPut(1);
        }else{
            list.get(updatePosition).setIsPut(0);
        }
        adapter.notifyItemChanged(updatePosition);
    }

    @Override
    public void upDateError(String msg) {

    }
    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this.getContext(),"玩命加载中...");
        dialog.show();
    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }
}
