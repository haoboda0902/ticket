package com.hbd.ticketscheck.shoptype.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;

import java.util.List;

public class ShopTypeAdapter extends RecyclerView.Adapter<ShopTypeAdapter.ViewHolder> {

    private List<ShopTypeBean.ProductListBean> listBeans;
    private Context context;
    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick){
        this.onItemClick = onItemClick;
    }


    public interface OnItemClick{

        void onItemClick(int position);
    }

    public ShopTypeAdapter(List<ShopTypeBean.ProductListBean> listBeans, Context context) {
        this.listBeans = listBeans;
        this.context = context;
    }

    @NonNull
    @Override
    public ShopTypeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_shop_type,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopTypeAdapter.ViewHolder holder, int position) {

        holder.name.setText(listBeans.get(position).getTypeName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listBeans == null ? 0 : listBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.shop_name);
        }
    }
}
