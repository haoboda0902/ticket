package com.hbd.ticketscheck.shop.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.shop.bean.LampListBean;

import java.util.List;

public interface LampView extends IBaseView {

    void getDate(List<String> data);

    void getList(LampListBean listBeans);

    void upDateSuccess();

    void upDateError(String msg);
}
