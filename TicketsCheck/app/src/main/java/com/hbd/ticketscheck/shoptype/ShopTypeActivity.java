package com.hbd.ticketscheck.shoptype;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shoptype.adapter.ShopTypeAdapter;
import com.hbd.ticketscheck.shoptype.iview.ShopTypeView;
import com.hbd.ticketscheck.shoptype.presenter.ShopTypePresenter;
import com.hbd.ticketscheck.view.LoadingDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShopTypeActivity extends BaseMvpActivity<ShopTypePresenter> implements ShopTypeView {

    private ImageView back;
    private RecyclerView rvTypeList;
    private TextView tvAdd;
    private EditText etNmae;
    private Button left,right;
    private Button save;

    private List<ShopTypeBean.ProductListBean> listBeans;
    private ShopTypeAdapter adapter;
    private int id = 0;
    private LoadingDialog dialog;

    @Override
    protected void initView() {
        back = findViewById(R.id.back);
        rvTypeList = findViewById(R.id.shoptype_list);
        tvAdd = findViewById(R.id.shoptype_add);
        etNmae = findViewById(R.id.shoptype_name);
        left = findViewById(R.id.btn_left);
        right = findViewById(R.id.btn_right);
        save = findViewById(R.id.btn_commit);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });

        initAdapter();
        mPresenter.getProductTypeList();
    }

    private void initAdapter() {
        listBeans = new ArrayList<>();
        adapter = new ShopTypeAdapter(listBeans,context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        rvTypeList.setLayoutManager(linearLayoutManager);
        rvTypeList.setAdapter(adapter);
        adapter.setOnItemClick(new ShopTypeAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position) {
                upDateShopType(position);
            }
        });
        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id = -1;
                etNmae.setText("");
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String typeName;
                if(etNmae.getText().toString().trim().length() <= 0){
                    Toast.makeText(context,"请输入名称",Toast.LENGTH_SHORT).show();
                    return;
                }
                typeName = etNmae.getText().toString().trim();

                    Map<String,Object> param = new HashMap<>();
                    param.put("typeName",typeName);
                if(id != -1){
                    param.put("id",id);
                }
                    mPresenter.productTypeupdateOrAdd(param);

            }
        });
    }

    private void upDateShopType(int position) {
        id = listBeans.get(position).getId();
        etNmae.setText(listBeans.get(position).getTypeName());
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position == 0){
                    return;
                }
                Map<String,Object> param = new HashMap<>();
                param.put("Id1",listBeans.get(position).getId());
                param.put("Id2",listBeans.get(position - 1).getId());
                mPresenter.moveProductType(param);
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position == listBeans.size() -1){
                    return;
                }

                Map<String,Object> param = new HashMap<>();
                param.put("Id1",listBeans.get(position).getId());
                param.put("Id2",listBeans.get(position + 1).getId());
                mPresenter.moveProductType(param);
            }
        });

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_shoptype;
    }

    @Override
    protected ShopTypePresenter createPresenter() {
        return new ShopTypePresenter(this);
    }

    @Override
    public void getShopSuccess(ShopTypeBean shopTypeBeans) {
        listBeans.clear();
        listBeans.addAll(shopTypeBeans.getProductList());
        adapter.notifyDataSetChanged();
        if(listBeans.size() > 0){
            id = listBeans.get(0).getId();
            etNmae.setText(listBeans.get(0).getTypeName());
        }
    }

    @Override
    public void getUpdateShopSuccess() {
        mPresenter.getProductTypeList();
    }

    @Override
    public void getUpdateShopError(String msg) {

    }

    @Override
    public void moveShopSuccess() {
        mPresenter.getProductTypeList();
    }

    @Override
    public void moveShopError(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this,"玩命加载中...");
        dialog.show();
    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }

}
