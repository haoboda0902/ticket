package com.hbd.ticketscheck.shop.bean;

import java.io.Serializable;

public class UpLoadBean implements Serializable {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
