package com.hbd.ticketscheck.orderstatistics.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.orderstatistics.bean.SumOrder;
import com.hbd.ticketscheck.orderstatistics.view.StatisticsView;

import java.util.Map;

public class StatisticsPresenter extends MyBasePresenter<StatisticsView> {

    public StatisticsPresenter(StatisticsView statisticsView){
        attachView(statisticsView);
    }

    public void search(Map<String, Object> params) {

        iView.showLoading();
        subscribe(apiServer.sumOrder(params), new BaseObserver<BaseModel<SumOrder>>(iView) {

            @Override
            public void onSuccess(BaseModel<SumOrder> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                SumOrder storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.getDataSuccess(storeBean);
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.hideLoading();
                iView.getdataError(msg);
            }
        });

    }
}
