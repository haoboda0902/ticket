package com.hbd.ticketscheck;

import java.io.Serializable;

public class SongListBean implements Serializable {
    private String createTime;
    private String descri;
    private int id;
    private String picUrl;
    private int sortIndex;
    private String submitTime;
    private int timeLong;
    private String title;
    private String videoUrl;
    private String musicUrl;
    private int type;
    private String musicTime;
    private boolean isPlaying;

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    public String getMusicTime() {
        return musicTime;
    }

    public void setMusicTime(String musicTime) {
        this.musicTime = musicTime;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDescri() {
        return descri;
    }

    public void setDescri(String descri) {
        this.descri = descri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public int getSortIndex() {
        return sortIndex;
    }

    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public int getTimeLong() {
        return timeLong;
    }

    public void setTimeLong(int timeLong) {
        this.timeLong = timeLong;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public String toString() {
        return "SongListBean{" +
                "createTime='" + createTime + '\'' +
                ", descri='" + descri + '\'' +
                ", id=" + id +
                ", picUrl='" + picUrl + '\'' +
                ", sortIndex=" + sortIndex +
                ", submitTime='" + submitTime + '\'' +
                ", timeLong=" + timeLong +
                ", title='" + title + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                '}';
    }
}
