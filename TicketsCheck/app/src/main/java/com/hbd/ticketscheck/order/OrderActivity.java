package com.hbd.ticketscheck.order;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpActivity;
import com.hbd.network.basepresenter.BasePresenter;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.order.adapter.OrderListAdapter;
import com.hbd.ticketscheck.order.bean.OrderBean;
import com.hbd.ticketscheck.order.iview.IOrderView;
import com.hbd.ticketscheck.order.presenter.OrderPresenter;
import com.hbd.ticketscheck.orderupdate.OrderUpdateActivity;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderActivity extends BaseMvpActivity<OrderPresenter> implements IOrderView {

    private SmartRefreshLayout smartRefreshLayout;
    private RecyclerView recyclerView;
    private ImageView back;

    private List<OrderBean.OrdersBean> list;
    private OrderListAdapter orderListAdapter;

    private int pageNo = 1;
    private int pageSize = 10;
    private int total = 0;

    private int updatePosition = -1;

    @Override
    protected void initView() {
        list = new ArrayList<>();
        orderListAdapter = new OrderListAdapter(context,list);
        recyclerView = findViewById(R.id.ry_order_list);
        back = findViewById(R.id.back);
        smartRefreshLayout = findViewById(R.id.smartrefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(activity));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(activity));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {

                pageNo = 1;
                smartRefreshLayout.setEnableLoadMore(true);//启用加载
                initList();
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                initList();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(orderListAdapter);
        orderListAdapter.setOnItemClick(new OrderListAdapter.OnItemClick() {
            @Override
            public void onItemCommitClick(int position) {
                if(list.get(position).getOrderState() == 2) {
                    Intent intent = new Intent(activity, OrderUpdateActivity.class);
                    intent.putExtra("data", (Serializable) list.get(position));
                    startActivity(intent);
                }else if(list.get(position).getOrderState() == 5){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                    alertDialog.setTitle("是否同意买家退货").setPositiveButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                        }
                    }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            updatePosition = position;
                            mPresenter.opeOrderForAdmin(list.get(position).getOrderNo(),1);
                        }
                    }).show();


                }else if(list.get(position).getOrderState() == 7){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
                    alertDialog.setTitle("是否收到退货商品").setPositiveButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            updatePosition = position;
                            mPresenter.opeOrderForAdmin(list.get(position).getOrderNo(),2);
                        }
                    }).show();
                }
            }
        });
        initList();
    }

    private void initList() {

        mPresenter.getOrderlist(pageNo,pageSize);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_order;
    }

    @Override
    protected OrderPresenter createPresenter() {
        return new OrderPresenter(this);
    }

    @Override
    public void dataSuccess(OrderBean orderBean) {
        total = orderBean.getTotal();
        if(pageNo == 1){
            list.clear();
            smartRefreshLayout.finishRefresh();
        }else {
            smartRefreshLayout.finishLoadMore();
        }
        pageNo++;
        list.addAll(orderBean.getOrders());
        orderListAdapter.notifyDataSetChanged();
        if(list.size() == total){
            smartRefreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void dataError(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateSuccess() {
        if(updatePosition != -1){
            list.get(updatePosition).setOrderState(8);
            orderListAdapter.notifyItemChanged(updatePosition);
        }
    }

    @Override
    public void updateError(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
