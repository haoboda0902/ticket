package com.hbd.ticketscheck.company.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.company.bean.CompanyBean;
import com.hbd.ticketscheck.company.view.CompanyView;
import com.hbd.ticketscheck.login.bean.UserBean;
import com.hbd.ticketscheck.login.iview.LoginView;
import com.hbd.ticketscheck.shop.bean.LampListBean;
import com.tencent.mmkv.MMKV;

import java.util.HashMap;
import java.util.Map;

public class CompanyPresenter extends MyBasePresenter<CompanyView> {

    public CompanyPresenter(CompanyView mainView) {
        attachView(mainView);
    }
    public void getMyTravelList(int pageNo,int pageSize){
        Map<String,Object> params = new HashMap<>();
        params.put("pageNo",pageNo);
        params.put("pageSize",pageSize);
        iView.showLoading();
        subscribe(apiServer.getMyTravelList(params), new BaseObserver<BaseModel<CompanyBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<CompanyBean> baseModel) {
                LogUtils.d(baseModel.toString());
                CompanyBean lampListBean = baseModel.getBody();

                if(lampListBean != null) {

                    iView.getData(lampListBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
            }
        });
    }

    public void myTravelupdateOrAdd(Map<String, Object> params) {
        iView.showLoading();
        subscribe(apiServer.myTravelupdateOrAdd(params), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                Object storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.updateSuccess();
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.hideLoading();
                iView.updateError(msg);
                LogUtils.d(msg.toString());
            }
        });
    }
}
