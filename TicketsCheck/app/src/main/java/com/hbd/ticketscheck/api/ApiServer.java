package com.hbd.ticketscheck.api;

import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.company.bean.CompanyBean;
import com.hbd.ticketscheck.erqode.bean.CheckCodeBean;
import com.hbd.ticketscheck.login.bean.UserBean;
import com.hbd.ticketscheck.order.bean.OrderBean;
import com.hbd.ticketscheck.orderstatistics.bean.SumOrder;
import com.hbd.ticketscheck.orderupdate.bean.OrderItems;
import com.hbd.ticketscheck.shop.bean.LampListBean;
import com.hbd.ticketscheck.shop.bean.MetrailBean;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shop.bean.StoreBean;
import com.hbd.ticketscheck.shop.bean.UpLoadBean;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;

public interface ApiServer {

    @FormUrlEncoded
    @POST("/app/manage/check/loginByToken.do")
    Observable<BaseModel<UserBean>> loginToken(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("/app/manage/check/checkOrder.do")
    Observable<BaseModel<Object>> checkOrder(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("/app/manage/check/getVerCode.do")
    Observable<BaseModel<Object>> getVerCode(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("app/manage/check/loginByVerCode.do")
    Observable<BaseModel<UserBean>> login(@FieldMap Map<String,String> map);

    @GET("/app/admin/myProduct/getProductList.do")
    Observable<BaseModel<MetrailBean>> getProductList(@QueryMap Map<String,Object> map);

    @FormUrlEncoded
    @POST("app/manage/check/getOrderByOrderNo.do")
    Observable<BaseModel<CheckCodeBean>> getOrderByOrderNo(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("/app/admin/myProductOrder/getProductOrderListByApp.do")
    Observable<BaseModel<OrderBean>> getOrderlist(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("/app/admin/myProductOrder/opeOrderForAdmin.do")
    Observable<BaseModel<Object>> opeOrderForAdmin(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("/app/admin/myProductOrder/getProductOrderItemForApp.do")
    Observable<BaseModel<OrderItems>> getProductOrderItemForApp(@FieldMap Map<String,String> map);

    @FormUrlEncoded
    @POST("/app/admin/myProductOrder/sendProductForAdmin.do")
    Observable<BaseModel<Object>> sendProductForAdmin(@FieldMap Map<String,String> map);

    @Multipart
    @POST("/app/upload/fileUpload.do")
    Observable<BaseModel<UpLoadBean>> upLoad(@Part MultipartBody.Part file);
    @FormUrlEncoded
    @POST("/app/admin/myGoods/getGoodsListByApp.do")
    Observable<BaseModel<LampListBean>> getGoodsListByApp(@FieldMap Map<String, Object> params);

    @GET("/app/admin/myGoods/getGoodsDateList.do")
    Observable<BaseModel<String[]>> getGoodsDateList();

    @GET("/app/admin/productType/getProductTypeList.do")
    Observable<BaseModel<ShopTypeBean>> getProductTypeList();

    @GET("/app/admin/myStore/getMyStoreList.do")
    Observable<BaseModel<StoreBean>> getMyStoreList();

    @FormUrlEncoded
    @POST("/app/admin/myProduct/updateOrAdd.do")
    Observable<BaseModel<Object>> updateOrAdd(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("/app/admin/myGoods/updateOrAdd.do")
    Observable<BaseModel<Object>> lampupdateOrAdd(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("/app/admin/myTravel/getMyTravelList.do")
    Observable<BaseModel<CompanyBean>> getMyTravelList(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("/app/admin/productType/updateOrAdd.do")
    Observable<BaseModel<Object>> productTypeupdateOrAdd(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("/app/admin/productType/moveProductType.do")
    Observable<BaseModel<Object>> moveProductType(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("/app/admin/myStore/updateOrAdd.do")
    Observable<BaseModel<Object>> myStoreupdateOrAdd(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("/app/admin/myTravel/updateOrAdd.do")
    Observable<BaseModel<Object>> myTravelupdateOrAdd(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("/app/admin/myGoodsOrderAdmin/sumOrder.do")
    Observable<BaseModel<SumOrder>> sumOrder(@FieldMap Map<String, Object> params);

}
