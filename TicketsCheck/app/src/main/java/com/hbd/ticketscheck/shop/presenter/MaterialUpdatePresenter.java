package com.hbd.ticketscheck.shop.presenter;

import android.util.Log;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.shop.bean.MetrailBean;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shop.bean.StoreBean;
import com.hbd.ticketscheck.shop.bean.UpLoadBean;
import com.hbd.ticketscheck.shop.iview.MaterialUpdateView;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MaterialUpdatePresenter extends MyBasePresenter<MaterialUpdateView> {

    public MaterialUpdatePresenter(MaterialUpdateView materialView) {
        attachView(materialView);
    }

    public void upLoad(String path, int type) {
        iView.showLoading();
        File file = new File(path);
        Log.e("上传文件",file.getTotalSpace()+"");
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/octet-stream"),file);
        MultipartBody.Part body =  MultipartBody.Part.createFormData("file",file.getName(),requestBody);
        subscribe(apiServer.upLoad(body), new BaseObserver<BaseModel<UpLoadBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<UpLoadBean> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                UpLoadBean upLoadBean = baseModel.getBody();

                if(upLoadBean != null) {

                    iView.upLoadSuccess(type,upLoadBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.hideLoading();
                LogUtils.d(msg.toString());
            }
        });
    }

    public void getProductTypeList() {
        subscribe(apiServer.getProductTypeList(), new BaseObserver<BaseModel<ShopTypeBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<ShopTypeBean> baseModel) {
                LogUtils.d(baseModel.toString());
                ShopTypeBean shopTypeBeans = baseModel.getBody();

                if(shopTypeBeans != null) {

                    iView.getShopSuccess(shopTypeBeans);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                iView.hideLoading();
                LogUtils.d(msg.toString());
            }
        });
    }

    public void getMyStoreList() {
        subscribe(apiServer.getMyStoreList(), new BaseObserver<BaseModel<StoreBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<StoreBean> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                StoreBean storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.getMyStoreList(storeBean);
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
            }
        });
    }

    public void updateOrAdd(Map<String,Object> param) {

        iView.showLoading();
        subscribe(apiServer.updateOrAdd(param), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                Object storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.upDateSuccess();
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.hideLoading();
                iView.upDateError(msg);
            }
        });
    }


}
