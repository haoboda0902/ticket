package com.hbd.ticketscheck.shoptype.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;

public interface ShopTypeView extends IBaseView {

    void getShopSuccess(ShopTypeBean shopTypeBeans);

    void getUpdateShopSuccess();

    void getUpdateShopError(String msg);

    void moveShopSuccess();

    void moveShopError(String msg);
}
