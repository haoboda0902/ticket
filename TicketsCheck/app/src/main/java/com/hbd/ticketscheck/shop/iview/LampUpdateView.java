package com.hbd.ticketscheck.shop.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.shop.bean.UpLoadBean;

public interface LampUpdateView extends IBaseView {

    void upDateSuccess();

    void upLoadSuccess(UpLoadBean upLoadBean);

    void upDateError(String msg);
}
