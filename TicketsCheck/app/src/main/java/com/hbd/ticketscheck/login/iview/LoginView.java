package com.hbd.ticketscheck.login.iview;

import com.hbd.network.baseview.IBaseView;

public interface LoginView extends IBaseView {

    void login();
    void codeSuccess();
    void codeError(String msg);
    void loginError(String msg);
}
