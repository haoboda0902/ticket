package com.hbd.ticketscheck.welcome;

import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;

import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.login.LoginActivity;
import com.hbd.ticketscheck.main.MainActivity;
import com.hbd.ticketscheck.main.NewMainActivity;
import com.hbd.ticketscheck.welcome.iview.IWelcomeView;
import com.hbd.ticketscheck.welcome.presenter.WelcomePresenter;
import com.tencent.mmkv.MMKV;


public class WelcomeActivity extends BaseMvpActivity<WelcomePresenter> implements IWelcomeView {

    @Override
    protected void initView() {
        MMKV kv = MMKV.defaultMMKV();
        String toekn = kv.decodeString("token");
        String userid = kv.decodeString("userid");
        if(!TextUtils.isEmpty(toekn) && !TextUtils.isEmpty(userid)) {
            mPresenter.loginToken(toekn,userid);
        }else{
            login();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_welcome;
    }

    @Override
    protected WelcomePresenter createPresenter() {
        return new WelcomePresenter(this);
    }

    @Override
    public void loginSuccess() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(WelcomeActivity.this, NewMainActivity.class);
                startActivity(intent);
                WelcomeActivity.this.finish();
            }
        },1000);
    }

    @Override
    public void loginError(String msg) {

        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        login();

    }

    private void login() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(intent);
                WelcomeActivity.this.finish();
            }
        },1000);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
