package com.hbd.ticketscheck.orderstatistics.adapter;

import android.app.LauncherActivity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.orderstatistics.bean.SumOrder;

import java.util.List;

public class SumAdapter extends RecyclerView.Adapter<SumAdapter.ViewHolder> {

    private List<SumOrder.GoodsListBean> listBeans;
    private Context context;

    public SumAdapter(List<SumOrder.GoodsListBean> listBeans, Context context) {
        this.listBeans = listBeans;
        this.context = context;
    }

    @NonNull
    @Override
    public SumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_sumorder,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SumAdapter.ViewHolder holder, int position) {
        holder.price.setText(listBeans.get(position).getGoodsPrice());
        holder.order_num.setText(listBeans.get(position).getOrderNum());
        holder.goods_num.setText(listBeans.get(position).getGoodsNum());
        holder.sum_price.setText(listBeans.get(position).getSumPrice());
    }

    @Override
    public int getItemCount() {
        return listBeans == null ? 0 :listBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView price,order_num,goods_num,sum_price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.price);
            order_num = itemView.findViewById(R.id.order_num);
            goods_num = itemView.findViewById(R.id.goods_num);
            sum_price = itemView.findViewById(R.id.sum_price);
        }
    }
}
