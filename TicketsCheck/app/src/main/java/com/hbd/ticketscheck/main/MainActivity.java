package com.hbd.ticketscheck.main;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.hbd.network.BaseMvpActivity;
import com.hbd.network.basepresenter.BasePresenter;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.erqode.ErQodeActivity;
import com.hbd.ticketscheck.login.LoginActivity;
import com.hbd.ticketscheck.welcome.WelcomeActivity;
import com.tencent.mmkv.MMKV;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends BaseMvpActivity implements  EasyPermissions.PermissionCallbacks  {


    private static final int REQUEST_CODE_QRCODE_PERMISSIONS = 1;
    private Button btnBommit;
    private ImageView imLoginOut;

    @Override
    protected void initView() {
        btnBommit = findViewById(R.id.btn_main_commit);
        imLoginOut = findViewById(R.id.im_login_out);
        btnBommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestCodeQRCodePermissions();

            }
        });
        imLoginOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this).setTitle("确认退出吗？")
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 点击“确认”后的操作
                                MainActivity.this.finish();
                                MMKV mmkv = MMKV.defaultMMKV();
                                mmkv.removeValueForKey("token");
                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("返回", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 点击“返回”后的操作,这里不设置没有任何操作
                            }
                        }).show();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected BasePresenter createPresenter() {
        return null;
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        // 授予权限

        gotoLogin();

    }

    private void gotoLogin() {
        Intent intent = new Intent(MainActivity.this, ErQodeActivity.class);
        startActivity(intent);

    }


    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        // 请求权限被拒
        Toast.makeText(this,"请在设置里同意用户权限",Toast.LENGTH_SHORT).show();
    }

    @AfterPermissionGranted(REQUEST_CODE_QRCODE_PERMISSIONS)
    private void requestCodeQRCodePermissions() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, "扫描二维码需要打开相机和闪光灯的权限", REQUEST_CODE_QRCODE_PERMISSIONS, perms);
        }else{
            gotoLogin();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
