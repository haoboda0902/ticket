package com.hbd.ticketscheck.company.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.company.bean.CompanyBean;
import com.hbd.ticketscheck.store.adapter.StoreAdapter;

import java.util.List;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {

    private List<CompanyBean.ProductListBean> list;
    private Context context;

    private StoreAdapter.OnItemClick onItemClick;

    public void setOnItemClick(StoreAdapter.OnItemClick onItemClick){
        this.onItemClick = onItemClick;
    }


    public interface OnItemClick{

        void onItemClick(int position);
    }


    public CompanyAdapter(List<CompanyBean.ProductListBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public CompanyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_company,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyAdapter.ViewHolder holder, int position) {
        holder.phone.setText(list.get(position).getTravelMobile());
        holder.num.setText(list.get(position).getTravelNo());
        holder.ids.setText(list.get(position).getId()+"");
        holder.name.setText(list.get(position).getTravelName());
        holder.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView num,ids,name,phone,action;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            num = itemView.findViewById(R.id.num);
            ids = itemView.findViewById(R.id.id_num);
            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.phone);
            action = itemView.findViewById(R.id.action);
        }
    }
}
