package com.hbd.ticketscheck.store.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.StoreBean;

import java.util.List;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.ViewHolder> {

    private List<StoreBean.StoreListBean> list;
    private Context context;

    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick){
        this.onItemClick = onItemClick;
    }


    public interface OnItemClick{

        void onItemClick(int position);
    }


    public StoreAdapter(List<StoreBean.StoreListBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public StoreAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_store,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreAdapter.ViewHolder holder, int position) {

        holder.store_name.setText(list.get(position).getStoreName());
        holder.store_admin_name.setText(list.get(position).getManagerName());
        holder.store_admin_phone.setText(list.get(position).getManagerMobile());
        holder.store_admin_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 :list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView store_name,store_admin_name,store_admin_phone;
        Button store_admin_update;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            store_name = itemView.findViewById(R.id.store_name);
            store_admin_name = itemView.findViewById(R.id.store_admin_name);
            store_admin_phone = itemView.findViewById(R.id.store_admin_phone);
            store_admin_update = itemView.findViewById(R.id.store_admin_update);

        }
    }
}
