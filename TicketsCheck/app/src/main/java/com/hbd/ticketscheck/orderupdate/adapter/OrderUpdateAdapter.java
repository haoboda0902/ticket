package com.hbd.ticketscheck.orderupdate.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.orderupdate.bean.OrderItems;
import com.hbd.ticketscheck.utils.GlideUtils;

import java.util.List;

public class OrderUpdateAdapter extends RecyclerView.Adapter<OrderUpdateAdapter.ViewHolder> {

    private Context context;
    private List<OrderItems.OrderItemsBean> list;

    public OrderUpdateAdapter(Context context, List<OrderItems.OrderItemsBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_order_update,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(list.get(position).getProductName());
        holder.des.setText(list.get(position).getDesc());
        holder.price.setText(list.get(position).getProductPrice()+"元");
        holder.num.setText(list.get(position).getOrderNum()+"");
        GlideUtils.loadUrlImage(context,list.get(position).getPic(),holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 :list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView;
        private TextView name,des,price,num;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.order_image);
            name = itemView.findViewById(R.id.order_name);
            des = itemView.findViewById(R.id.order_des);
            price = itemView.findViewById(R.id.order_price);
            num = itemView.findViewById(R.id.order_num);
        }
    }
}
