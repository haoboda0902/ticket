package com.hbd.ticketscheck.shop.bean;

import java.io.Serializable;
import java.util.List;

public class LampListBean {


    /**
     * total : 17
     * goodsList : [{"createDate":"2020-09-27 11:16:57","createUser":"system","desc":"","goodsDes":"愿灯1盏","goodsInfo":"","goodsName":"参与灯赞五台山","goodsPrice":88,"goodsType":1,"id":11,"isPut":0,"lastSaldate":"2020-09-30 19:15:20","lastSaldateStr":"","lightNum":0,"oldPrice":0,"pic":"http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg","showDate":"2020-09-30","showDateStr":"","showTime":"19:10 ~ 19:50","totalNum":1000,"updateDate":"","updateUser":"","useNum":0},{"createDate":"2020-09-27 11:16:57","createUser":"system","desc":"","goodsDes":"愿灯1盏+福灯1盏","goodsInfo":"","goodsName":"参与灯赞五台山","goodsPrice":168,"goodsType":1,"id":12,"isPut":0,"lastSaldate":"2020-09-30 19:15:20","lastSaldateStr":"","lightNum":0,"oldPrice":0,"pic":"http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg","showDate":"2020-09-30","showDateStr":"","showTime":"19:10 ~ 19:50","totalNum":1000,"updateDate":"","updateUser":"","useNum":0},{"createDate":"2020-09-27 11:16:57","createUser":"system","desc":"","goodsDes":"愿灯1盏","goodsInfo":"","goodsName":"参与灯赞五台山","goodsPrice":88,"goodsType":1,"id":13,"isPut":0,"lastSaldate":"2020-10-01 19:55:20","lastSaldateStr":"","lightNum":0,"oldPrice":0,"pic":"http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg","showDate":"2020-10-01","showDateStr":"","showTime":"19:50 ~ 20:30","totalNum":1000,"updateDate":"","updateUser":"","useNum":7},{"createDate":"2020-09-27 11:16:57","createUser":"system","desc":"","goodsDes":"愿灯1盏+福灯1盏","goodsInfo":"","goodsName":"参与灯赞五台山","goodsPrice":168,"goodsType":1,"id":14,"isPut":0,"lastSaldate":"2020-09-30 19:55:20","lastSaldateStr":"","lightNum":0,"oldPrice":0,"pic":"http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg","showDate":"2020-10-01","showDateStr":"","showTime":"19:50 ~ 20:30","totalNum":1000,"updateDate":"","updateUser":"","useNum":0},{"createDate":"2020-09-27 11:16:57","createUser":"system","desc":"","goodsDes":"愿灯1盏","goodsInfo":"","goodsName":"团体票","goodsPrice":88,"goodsType":2,"id":15,"isPut":0,"lastSaldate":"2020-09-30 19:15:16","lastSaldateStr":"","lightNum":0,"oldPrice":0,"pic":"http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg","showDate":"2020-09-30","showDateStr":"","showTime":"19:10 ~ 19:50","totalNum":1000,"updateDate":"","updateUser":"","useNum":0},{"createDate":"2020-09-27 11:16:57","createUser":"system","desc":"","goodsDes":"愿灯1盏+福灯1盏1","goodsInfo":"","goodsName":"愿灯1盏+福灯1盏1","goodsPrice":168,"goodsType":2,"id":16,"isPut":0,"lastSaldate":"2020-09-30 19:15:15","lastSaldateStr":"","lightNum":0,"oldPrice":0,"pic":"http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg","showDate":"2020-09-30","showDateStr":"","showTime":"19:10 ~ 19:50","totalNum":1000,"updateDate":"2020-10-02 17:07:21","updateUser":"system","useNum":0},{"createDate":"2020-10-02 18:33:18","createUser":"system","desc":"","goodsDes":"愿灯1盏+福灯1盏2","goodsInfo":"","goodsName":"愿灯1盏+福灯1盏2","goodsPrice":168,"goodsType":1,"id":29,"isPut":0,"lastSaldate":"2020-10-30 18:33:04","lastSaldateStr":"","lightNum":0,"oldPrice":0,"pic":"http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg","showDate":"2020-10-30","showDateStr":"","showTime":"19:10 ~ 19:50","totalNum":1000,"updateDate":"","updateUser":"","useNum":0}]
     */

    private int total;
    private List<GoodsListBean> goodsList;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public static class GoodsListBean implements Serializable {
        /**
         * createDate : 2020-09-27 11:16:57
         * createUser : system
         * desc :
         * goodsDes : 愿灯1盏
         * goodsInfo :
         * goodsName : 参与灯赞五台山
         * goodsPrice : 88.0
         * goodsType : 1
         * id : 11
         * isPut : 0
         * lastSaldate : 2020-09-30 19:15:20
         * lastSaldateStr :
         * lightNum : 0
         * oldPrice : 0.0
         * pic : http://scenicspotstatic.bjyidongtianqi.com/scenicspotH5/pd1.jpg
         * showDate : 2020-09-30
         * showDateStr :
         * showTime : 19:10 ~ 19:50
         * totalNum : 1000
         * updateDate :
         * updateUser :
         * useNum : 0
         */

        private String createDate;
        private String createUser;
        private String desc;
        private String goodsDes;
        private String goodsInfo;
        private String goodsName;
        private double goodsPrice;
        private int goodsType;
        private int id;
        private int isPut;
        private String lastSaldate;
        private String lastSaldateStr;
        private int lightNum;
        private double oldPrice;
        private String pic;
        private String showDate;
        private String showDateStr;
        private String showTime;
        private int totalNum;
        private String updateDate;
        private String updateUser;
        private int useNum;

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getGoodsDes() {
            return goodsDes;
        }

        public void setGoodsDes(String goodsDes) {
            this.goodsDes = goodsDes;
        }

        public String getGoodsInfo() {
            return goodsInfo;
        }

        public void setGoodsInfo(String goodsInfo) {
            this.goodsInfo = goodsInfo;
        }

        public String getGoodsName() {
            return goodsName;
        }

        public void setGoodsName(String goodsName) {
            this.goodsName = goodsName;
        }

        public double getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(double goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public int getGoodsType() {
            return goodsType;
        }

        public void setGoodsType(int goodsType) {
            this.goodsType = goodsType;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsPut() {
            return isPut;
        }

        public void setIsPut(int isPut) {
            this.isPut = isPut;
        }

        public String getLastSaldate() {
            return lastSaldate;
        }

        public void setLastSaldate(String lastSaldate) {
            this.lastSaldate = lastSaldate;
        }

        public String getLastSaldateStr() {
            return lastSaldateStr;
        }

        public void setLastSaldateStr(String lastSaldateStr) {
            this.lastSaldateStr = lastSaldateStr;
        }

        public int getLightNum() {
            return lightNum;
        }

        public void setLightNum(int lightNum) {
            this.lightNum = lightNum;
        }

        public double getOldPrice() {
            return oldPrice;
        }

        public void setOldPrice(double oldPrice) {
            this.oldPrice = oldPrice;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getShowDate() {
            return showDate;
        }

        public void setShowDate(String showDate) {
            this.showDate = showDate;
        }

        public String getShowDateStr() {
            return showDateStr;
        }

        public void setShowDateStr(String showDateStr) {
            this.showDateStr = showDateStr;
        }

        public String getShowTime() {
            return showTime;
        }

        public void setShowTime(String showTime) {
            this.showTime = showTime;
        }

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }

        public int getUseNum() {
            return useNum;
        }

        public void setUseNum(int useNum) {
            this.useNum = useNum;
        }
    }
}
