package com.hbd.ticketscheck.store.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shop.bean.StoreBean;

public interface StoreView extends IBaseView {

    void getStorreSuccess(StoreBean storeBean);

    void getStoreError(String msg);

    void updateSuccess();

    void updateError(String msg);
}
