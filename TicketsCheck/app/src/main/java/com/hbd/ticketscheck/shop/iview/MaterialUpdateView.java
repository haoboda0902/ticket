package com.hbd.ticketscheck.shop.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shop.bean.StoreBean;
import com.hbd.ticketscheck.shop.bean.UpLoadBean;

public interface MaterialUpdateView extends IBaseView {

    void upLoadSuccess(int type, UpLoadBean upLoadBean);

    void getShopSuccess(ShopTypeBean shopTypeBeans);

    void getMyStoreList(StoreBean storeBean);

    void upDateSuccess();

    void upDateError(String msg);
}
