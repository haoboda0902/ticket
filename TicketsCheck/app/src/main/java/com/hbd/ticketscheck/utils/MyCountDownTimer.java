package com.hbd.ticketscheck.utils;

import android.os.CountDownTimer;
import android.widget.TextView;

public class MyCountDownTimer extends CountDownTimer {

    public TextView textView;

    public MyCountDownTimer(long millisInFuture, long countDownInterval,TextView textView) {
        super(millisInFuture, countDownInterval);
        this.textView = textView;
    }

    @Override
    public void onTick(long l) {
        //防止计时过程中重复点击
        textView.setClickable(false);
        textView.setText(l/1000+"秒后获取");
    }

    @Override
    public void onFinish() {
        //重新给Button设置文字
        textView.setText("重新获取");
        //设置可点击
        textView.setClickable(true);
    }
}
