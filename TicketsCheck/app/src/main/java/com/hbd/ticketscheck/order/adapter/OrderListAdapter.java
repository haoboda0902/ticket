package com.hbd.ticketscheck.order.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.order.bean.OrderBean;
import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> {

    private Context context;
    private List<OrderBean.OrdersBean> list;

    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick){
        this.onItemClick = onItemClick;
    }


    public interface OnItemClick{

        void onItemCommitClick(int position);

    }

    public OrderListAdapter(Context context, List<OrderBean.OrdersBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public OrderListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_order_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListAdapter.ViewHolder holder, int position) {

        holder.orderPrice.setText("订单金额：" + list.get(position).getOrderMoney()+"元");
        holder.orderNum.setText("订单编号：" + list.get(position).getOrderNo());

        //订单状态：1：下单 2：订单支付成功，3：已发货 ,4: 已收货 ，5：申请退货 6：同意退货 7：退货中 8： 已完成
        if(list.get(position).getOrderState() == 5){
            holder.commit.setVisibility(View.VISIBLE);
            holder.commit.setText("退货处理");
        }else if(list.get(position).getOrderState() == 7){
            holder.commit.setVisibility(View.VISIBLE);
            holder.commit.setText("收到退货");
        }else if(list.get(position).getOrderState() ==2 ){
            holder.commit.setVisibility(View.VISIBLE);
            holder.commit.setText("订单处理");
        }else{
            holder.commit.setVisibility(View.GONE);
        }
        holder.commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemCommitClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 :list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView orderPrice,orderNum,commit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            orderPrice = itemView.findViewById(R.id.order_price);
            orderNum = itemView.findViewById(R.id.order_num);
            commit = itemView.findViewById(R.id.order_commit);
        }
    }
}
