package com.hbd.ticketscheck.order.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.login.bean.UserBean;
import com.hbd.ticketscheck.order.bean.OrderBean;
import com.hbd.ticketscheck.order.iview.IOrderView;
import com.tencent.mmkv.MMKV;

import java.util.HashMap;
import java.util.Map;

public class OrderPresenter extends MyBasePresenter<IOrderView> {

    public OrderPresenter(IOrderView iWelcomeView) {
        attachView(iWelcomeView);
    }

    public void getOrderlist(int pageNo,int pageSize){
        Map<String,String> params = new HashMap<>();
        params.put("pageNo", String.valueOf(pageNo));
        params.put("pageSize", String.valueOf(pageSize));
        iView.showLoading();
        subscribe(apiServer.getOrderlist(params), new BaseObserver<BaseModel<OrderBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<OrderBean> baseModel) {
                LogUtils.d(baseModel.toString());
                OrderBean orderBean = baseModel.getBody();
                if(orderBean != null) {
                    iView.dataSuccess(orderBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.dataError(msg);
            }
        });
    }

    public void opeOrderForAdmin(String orderNo,int ope){
        Map<String,String> params = new HashMap<>();
        params.put("orderNo", orderNo);
        params.put("ope", String.valueOf(ope));
        iView.showLoading();
        subscribe(apiServer.sendProductForAdmin(params), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                LogUtils.d(baseModel.toString());
                Object orderBean = baseModel.getBody();
                if(orderBean != null) {
                    iView.updateSuccess();
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.updateError(msg);
            }
        });
    }
}
