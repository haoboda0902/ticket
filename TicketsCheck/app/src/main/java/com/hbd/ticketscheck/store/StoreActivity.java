package com.hbd.ticketscheck.store;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.StoreBean;
import com.hbd.ticketscheck.store.adapter.StoreAdapter;
import com.hbd.ticketscheck.store.iview.StoreView;
import com.hbd.ticketscheck.store.presenter.StorePresenter;
import com.hbd.ticketscheck.view.LoadingDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoreActivity extends BaseMvpActivity<StorePresenter> implements StoreView {

    private ImageView back;
    private RecyclerView recyclerView;
    private Button btnAdd;

    private List<StoreBean.StoreListBean> listBeanList;
    private StoreAdapter adapter;
    private LoadingDialog dialog;

    @Override
    protected void initView() {
        back = findViewById(R.id.back);
        recyclerView = findViewById(R.id.rv_store_list);
        btnAdd = findViewById(R.id.btn_add);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StoreUpdateDialog storeUpdateDialog = new StoreUpdateDialog(context,activity);
                storeUpdateDialog.show();
            }
        });
        initAdapter();
    }

    private void initAdapter() {
        listBeanList = new ArrayList<>();
        adapter = new StoreAdapter(listBeanList,context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClick(new StoreAdapter.OnItemClick() {
            @Override
            public void onItemClick(int position) {
                StoreUpdateDialog storeUpdateDialog = new StoreUpdateDialog(context,activity);
                storeUpdateDialog.show();
                storeUpdateDialog.setData(listBeanList.get(position));
            }
        });

        mPresenter.getMyStoreList();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_store;
    }

    @Override
    protected StorePresenter createPresenter() {
        return new StorePresenter(this);
    }

    @Override
    public void getStorreSuccess(StoreBean storeBean) {
        listBeanList.clear();
        if(storeBean.getStoreList() != null){
            listBeanList.addAll(storeBean.getStoreList());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void getStoreError(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateSuccess() {
        mPresenter.getMyStoreList();
    }

    @Override
    public void updateError(String msg) {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this,"加载中...");
        dialog.show();
    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }

    public void saveStore(String storeName, String managerName, String managerAccount, String managerPassswd, String managerMobile, int id){
        Map<String,Object> params = new HashMap<>();
        if(id != -1){
            params.put("id",id);
        }

        params.put("storeName",storeName);
        params.put("managerName",managerName);
        params.put("managerAccount",managerAccount);
        params.put("managerPassswd",managerPassswd);
        params.put("managerMobile",managerMobile);

        mPresenter.myStoreupdateOrAdd(params);


    }
}
