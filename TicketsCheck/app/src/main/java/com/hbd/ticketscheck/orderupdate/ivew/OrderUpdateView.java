package com.hbd.ticketscheck.orderupdate.ivew;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.orderupdate.bean.OrderItems;

public interface OrderUpdateView extends IBaseView {

    void saveSuccess();

    void saveError(String msg);

    void getOrder(OrderItems orderItems);

    void getOrderError(String msg);

}
