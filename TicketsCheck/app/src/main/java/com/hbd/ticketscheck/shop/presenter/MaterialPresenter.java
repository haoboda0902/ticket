package com.hbd.ticketscheck.shop.presenter;

import com.hbd.network.LogUtils;
import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.basemodel.BaseModel;
import com.hbd.ticketscheck.api.MyBasePresenter;
import com.hbd.ticketscheck.login.bean.UserBean;
import com.hbd.ticketscheck.shop.bean.MetrailBean;
import com.hbd.ticketscheck.shop.iview.MaterialView;
import com.tencent.mmkv.MMKV;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MaterialPresenter extends MyBasePresenter<MaterialView> {

    public MaterialPresenter(MaterialView materialView) {
        attachView(materialView);
    }

    public void getDate(int pageNo,int pageSize){
        Map<String,Object> params = new HashMap<>();
        params.put("pageNo",pageNo);
        params.put("pageSize",pageSize);
        subscribe(apiServer.getProductList(params), new BaseObserver<BaseModel<MetrailBean>>(iView) {

            @Override
            public void onSuccess(BaseModel<MetrailBean> baseModel) {
                LogUtils.d(baseModel.toString());
                MetrailBean metrailBean = baseModel.getBody();

                if(metrailBean != null) {

                    iView.getDataSuccess(metrailBean);
                }else{
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
            }
        });
    }

    public void updateOrAdd(Map<String,Object> param) {

        iView.showLoading();
        subscribe(apiServer.updateOrAdd(param), new BaseObserver<BaseModel<Object>>(iView) {

            @Override
            public void onSuccess(BaseModel<Object> baseModel) {
                LogUtils.d(baseModel.toString());
                iView.hideLoading();
                Object storeBean = baseModel.getBody();

                if (storeBean != null) {

                    iView.upDateSuccess();
                } else {
                    onError(baseModel.getReturnTip());
                }
            }

            @Override
            public void onError(String msg) {
                LogUtils.d(msg.toString());
                iView.hideLoading();
                iView.upDateError(msg);
            }
        });
    }
}
