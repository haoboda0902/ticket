package com.hbd.ticketscheck.shop;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.MetrailBean;
import com.hbd.ticketscheck.shop.bean.ShopTypeBean;
import com.hbd.ticketscheck.shop.bean.StoreBean;
import com.hbd.ticketscheck.shop.bean.UpLoadBean;
import com.hbd.ticketscheck.shop.iview.MaterialUpdateView;
import com.hbd.ticketscheck.shop.presenter.MaterialUpdatePresenter;
import com.hbd.ticketscheck.utils.GlideEngine;
import com.hbd.ticketscheck.utils.GlideUtils;
import com.hbd.ticketscheck.view.LoadingDialog;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaterialUpdateActivity extends BaseMvpActivity<MaterialUpdatePresenter> implements MaterialUpdateView {

    private int type = 1; // 1  展示图片   2描述图片
    private ImageView back,iv_show,iv_des;
    private Spinner spinnerShop,spinnerStore;
    private String[] shopTypeName,storeTypeName;
    private EditText name,price,weight,nomalPostage,specialPostage;
    private Button material_add;

    private MetrailBean.ProductListBean productListBean;
    private ArrayAdapter<String> adapter,adapterStore;
    private int shopType,storeType;
    private String pic,desPic;

    private LoadingDialog dialog;
    @Override
    protected void initView() {
        getIntentData();
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaterialUpdateActivity.this.finish();
            }
        });
        iv_show = findViewById(R.id.iv_show);
        iv_des = findViewById(R.id.iv_des);
        spinnerShop = findViewById(R.id.shop_type);
        spinnerStore = findViewById(R.id.cangku_type);
        name = findViewById(R.id.shop_name);
        price = findViewById(R.id.shop_price);
        weight = findViewById(R.id.shop_weight);
        nomalPostage = findViewById(R.id.nomal_postage);
        specialPostage = findViewById(R.id.special_postage);
        material_add = findViewById(R.id.material_add);
        iv_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = 1;
                PictureSelector.create(MaterialUpdateActivity.this)
                        .openGallery(PictureMimeType.ofImage())
                        .loadImageEngine(GlideEngine.createGlideEngine())
                        .selectionMode(PictureConfig.SINGLE)
                        .imageFormat(PictureMimeType.PNG)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
            }

        });
        iv_des.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = 2;
                PictureSelector.create(MaterialUpdateActivity.this)
                        .openGallery(PictureMimeType.ofImage())
                        .loadImageEngine(GlideEngine.createGlideEngine())
                        .selectionMode(PictureConfig.SINGLE)
                        .imageFormat(PictureMimeType.PNG)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
            }

        });
        mPresenter.getProductTypeList();
        mPresenter.getMyStoreList();

        material_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });

        initData();

    }

    private void initData() {

        if(productListBean != null){
            name.setText(productListBean.getProductName());
            price.setText(productListBean.getProductPrice()+"");
            weight.setText(productListBean.getProductWeight()+"");
            nomalPostage.setText(productListBean.getCommonPostage()+"");
            specialPostage.setText(productListBean.getSpecialPostage()+"");
            pic = productListBean.getPic();

            GlideUtils.loadUrlImage(context,pic,iv_show);
            desPic = productListBean.getDesPic();
            GlideUtils.loadUrlImage(context,desPic,iv_des);
        }

    }

    private void update() {
        if(name.getText().toString().trim().length() <1){
            Toast.makeText(context,"请输入商品标题",Toast.LENGTH_SHORT).show();
            return;
        }
        if(price.getText().toString().trim().length() <1){
            Toast.makeText(context,"请输入商品价格",Toast.LENGTH_SHORT).show();
            return;
        }
        if(weight.getText().toString().trim().length() <1){
            Toast.makeText(context,"请输入商品重量",Toast.LENGTH_SHORT).show();
            return;
        }
        if(nomalPostage.getText().toString().trim().length() <1){
            Toast.makeText(context,"请输入商品邮费",Toast.LENGTH_SHORT).show();
            return;
        }
        if(specialPostage.getText().toString().trim().length() <1){
            Toast.makeText(context,"请输入商品特殊邮费",Toast.LENGTH_SHORT).show();
            return;
        }
        if(pic == null){
            Toast.makeText(context,"请选择商品图片",Toast.LENGTH_SHORT).show();
            return;
        }
        if(desPic == null){
            Toast.makeText(context,"请选择商品描述图片",Toast.LENGTH_SHORT).show();
            return;
        }
        String shopName = name.getText().toString().trim();
        String shopprice = price.getText().toString().trim();
        String shopweight = weight.getText().toString().trim();
        String shopnomalPostage = nomalPostage.getText().toString().trim();
        String shopspecialPostage = specialPostage.getText().toString().trim();

        Map<String,Object> param = new HashMap<>();
        param.put("productName",shopName);
        param.put("pic",pic);
        param.put("desPic",desPic);
        param.put("commonPostage",shopnomalPostage);
        param.put("productPrice",shopprice);
        param.put("productType",shopType);
        param.put("productWeight",shopweight);
        param.put("specialPostage",shopspecialPostage);
        param.put("storeId",storeType);

        int id ;
        if(productListBean != null){
            param.put("id",productListBean.getId());
        }
        mPresenter.updateOrAdd(param);
    }

    private void getIntentData() {

        productListBean = (MetrailBean.ProductListBean) getIntent().getSerializableExtra("data");

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_material_update;
    }

    @Override
    protected MaterialUpdatePresenter createPresenter() {
        return new MaterialUpdatePresenter(this);
    }

    @Override
    public void upLoadSuccess(int type, UpLoadBean upLoadBean) {
        if(type ==1){
            pic = upLoadBean.getUrl();
            GlideUtils.loadUrlImage(context,pic,iv_show);
        }else{
            desPic = upLoadBean.getUrl();
            GlideUtils.loadUrlImage(context,desPic,iv_des);
        }
    }

    @Override
    public void getShopSuccess(ShopTypeBean shopTypeBeans) {
        if(shopTypeBeans.getProductList() != null){
            shopTypeName = new String[shopTypeBeans.getProductList().size()];
            for (int i = 0; i < shopTypeBeans.getProductList().size(); i++) {
                if(shopType == shopTypeBeans.getProductList().get(i).getId()){

                }
                shopTypeName[i] = shopTypeBeans.getProductList().get(i).getTypeName();
            }
            adapter = new ArrayAdapter<String>(this,R.layout.layout_spinner,shopTypeName);

            //设置下拉列表的风格
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            //将adapter 添加到spinner中
            spinnerShop.setAdapter(adapter);
            for (int i = 0; i < shopTypeBeans.getProductList().size(); i++) {
                if(shopType == shopTypeBeans.getProductList().get(i).getId()){
                    spinnerShop.setSelection(i);
                }
            }

            //添加事件Spinner事件监听
            spinnerShop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    storeType = shopTypeBeans.getProductList().get(i).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    @Override
    public void getMyStoreList(StoreBean storeBean) {
        if(storeBean.getStoreList() != null){
            storeTypeName = new String[storeBean.getStoreList().size()];
            for (int i = 0; i < storeBean.getStoreList().size(); i++) {
                storeTypeName[i] = storeBean.getStoreList().get(i).getStoreName();
            }
            adapterStore = new ArrayAdapter<String>(this,R.layout.layout_spinner,storeTypeName);

            //设置下拉列表的风格
            adapterStore.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            //将adapter 添加到spinner中
            spinnerStore.setAdapter(adapterStore);
            for (int i = 0; i < storeBean.getStoreList().size(); i++) {
                if(storeType == storeBean.getStoreList().get(i).getId()){
                    spinnerStore.setSelection(i);
                }
            }
            //添加事件Spinner事件监听
            spinnerStore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    storeType = storeBean.getStoreList().get(i).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    @Override
    public void upDateSuccess() {
        this.finish();
    }

    @Override
    public void upDateError(String msg) {
        Toast.makeText(context,"更新失败",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        dialog = new LoadingDialog(this,"玩命加载中...");
        dialog.show();

    }

    @Override
    public void hideLoading() {
        if(dialog != null){
            dialog.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 结果回调
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    if(selectList != null && selectList.size() > 0){
                        String path = selectList.get(0).getRealPath();
                        upLoadPic(path);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void upLoadPic(String path) {

        mPresenter.upLoad(path,type);

    }
}
