package com.hbd.ticketscheck.shop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;

import java.util.List;

public class LampDateAdapter extends RecyclerView.Adapter<LampDateAdapter.ViewHolder> {
    private List<String> list;
    private Context context;

    public LampDateAdapter(List<String> list, Context context) {
        this.list = list;
        this.context = context;
    }

    private OnItemClickLisenter onItemClickLisenter;

    public void setOnItemClickLisenter(OnItemClickLisenter onItemClickLisenter){
        this.onItemClickLisenter = onItemClickLisenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_lamp_title,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.date.setText(list.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickLisenter.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 :list.size() ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.tv_lamp_data);
        }
    }

    public interface OnItemClickLisenter{

        void onClick(int position);
    }
}
