package com.hbd.ticketscheck.order.iview;

import com.hbd.network.baseview.IBaseView;
import com.hbd.ticketscheck.order.bean.OrderBean;

public interface IOrderView extends IBaseView {

    void dataSuccess(OrderBean orderBean);

    void dataError(String msg);


    void updateSuccess();

    void updateError(String msg);
}
