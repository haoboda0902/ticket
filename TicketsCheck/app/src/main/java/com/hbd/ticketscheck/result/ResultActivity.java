package com.hbd.ticketscheck.result;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hbd.network.BaseMvpActivity;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.erqode.bean.CheckCodeBean;
import com.hbd.ticketscheck.result.iview.IResultView;
import com.hbd.ticketscheck.result.presenter.ResultPresenter;
import com.hbd.ticketscheck.utils.DateUtil;
import com.tencent.mmkv.MMKV;

public class ResultActivity extends BaseMvpActivity<ResultPresenter> implements IResultView {


    private TextView tvState,tv_time,tv_show_time,tv_des,tv_price;
    private TextView tvNumPeople;
    private Button commit;
    private CheckCodeBean checkCodeBean;
    private String code;

    @Override
    protected void initView() {
        checkCodeBean = (CheckCodeBean) getIntent().getSerializableExtra("result");
        code = getIntent().getStringExtra("code");
        tvState = findViewById(R.id.tv_state);
        tvNumPeople = findViewById(R.id.tv_num_people);
        tv_time = findViewById(R.id.tv_time);
        tv_show_time = findViewById(R.id.tv_show_time);
        tv_des = findViewById(R.id.tv_des);
        tv_price = findViewById(R.id.tv_price);
        commit = findViewById(R.id.btn_commit);
        if(checkCodeBean != null){
            tvState.setText("成功");
            tvNumPeople.setText("可通过人数："+checkCodeBean.getOrderNum()+"人");
            tv_des.setText(checkCodeBean.getGoodsDes());
            String datge  = DateUtil.timeStamp2Date(String.valueOf(checkCodeBean.getShowDate().getTime()),"yyyy-MM-dd");
            tv_time.setText(datge);
            tv_show_time.setText(checkCodeBean.getShowTime());
            tv_price.setText(checkCodeBean.getProductPrice());

        }else{
            tvState.setText("失败");
        }

        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkCodeBean != null){
                    MMKV kv = MMKV.defaultMMKV();
                    String toekn = kv.decodeString("token");
                    String userid = kv.decodeString("userid");
                    String orderNo = checkCodeBean.getOrderNo();
                    if(!TextUtils.isEmpty(toekn) && !TextUtils.isEmpty(userid)) {
                        mPresenter.check(toekn,userid,code);
                    }

                }else{
                    ResultActivity.this.finish();
                }
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_result;
    }

    @Override
    protected ResultPresenter createPresenter() {
        return new ResultPresenter(this);
    }

    @Override
    public void success() {
        Toast.makeText(ResultActivity.this,"核销成功",Toast.LENGTH_LONG).show();
            this.finish();
    }

    @Override
    public void error(String msg) {
        if(!TextUtils.isEmpty(msg)) {
            Toast.makeText(ResultActivity.this, msg, Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(ResultActivity.this, "核销失败", Toast.LENGTH_LONG).show();
        }
        this.finish();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
