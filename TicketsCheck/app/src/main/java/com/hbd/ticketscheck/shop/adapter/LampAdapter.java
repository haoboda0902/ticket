package com.hbd.ticketscheck.shop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.shop.bean.LampListBean;
import com.hbd.ticketscheck.utils.GlideUtils;

import java.util.List;

public class LampAdapter extends RecyclerView.Adapter<LampAdapter.ViewHolder> {

    private List<LampListBean.GoodsListBean> list;
    private Context context;

    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick){
        this.onItemClick = onItemClick;
    }


    public interface OnItemClick{

        void onItemUpdateClick(int position);

        void onItemUpDownClick(int position);
    }

    public LampAdapter(List<LampListBean.GoodsListBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public LampAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_shop_lampl,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LampAdapter.ViewHolder holder, int position) {

        GlideUtils.loadUrlImage(context,list.get(position).getPic(),holder.imageView);
        GlideUtils.loadUrlImage(context,list.get(position).getPic(),holder.icon1);
        GlideUtils.loadUrlImage(context,list.get(position).getPic(),holder.icon2);
        holder.name.setText(list.get(position).getGoodsName());
        holder.time.setText(list.get(position).getShowDate());
        holder.price.setText(list.get(position).getGoodsPrice()+"元");
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemUpdateClick(position);
            }
        });

        if(list.get(position).getIsPut() == 0){
            holder.up.setText("上架");
        }else{
            holder.up.setText("下架");
        }

        holder.up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItemUpDownClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 :list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageView,icon1,icon2;
        private TextView time,name,price;
        private Button up,update;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.shop_lamp_img);
            icon1 = itemView.findViewById(R.id.image1);
            icon2 = itemView.findViewById(R.id.image2);
            time = itemView.findViewById(R.id.shop_lamp_time);
            name = itemView.findViewById(R.id.shop_lamp_name);
            price = itemView.findViewById(R.id.shop_lamp_price);
            up = itemView.findViewById(R.id.shop_lamp_up);
            update = itemView.findViewById(R.id.shop_update_update);
        }
    }
}
