package com.hbd.ticketscheck.order.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.order.bean.OrderBean;

import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private List<OrderBean> list;
    private Context context;

    public OrderAdapter(List<OrderBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_order,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder holder, int position) {
        Glide.with(context).load(R.mipmap.icon_shop_image).into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return list == null ? 5 :5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView des,price;
        ImageView icon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            des = itemView.findViewById(R.id.order_des);
            price = itemView.findViewById(R.id.order_price);
            icon = itemView.findViewById(R.id.order_image);
        }
    }
}
