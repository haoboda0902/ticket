package com.hbd.ticketscheck.erqode;

import android.content.Intent;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.hbd.network.BaseMvpActivity;
import com.hbd.network.LogUtils;
import com.hbd.network.basepresenter.BasePresenter;
import com.hbd.ticketscheck.R;
import com.hbd.ticketscheck.erqode.bean.CheckCodeBean;
import com.hbd.ticketscheck.erqode.iview.IErqodeView;
import com.hbd.ticketscheck.erqode.presenter.ErqodePresenter;
import com.hbd.ticketscheck.result.ResultActivity;
import com.tencent.mmkv.MMKV;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.bingoogolapple.qrcode.zxing.ZXingView;

import static android.icu.text.DateTimePatternGenerator.PatternInfo.OK;

public class ErQodeActivity extends BaseMvpActivity<ErqodePresenter> implements QRCodeView.Delegate , IErqodeView {

    private ZXingView mZXingView;
    private TextView tvOpenFlashlight;
    private boolean isOpen;
    private String code = null;

    private boolean isScan = false;

    @Override
    protected void initView() {
        isScan = getIntent().getBooleanExtra("data",false);
        mZXingView = findViewById(R.id.zxingview);
        tvOpenFlashlight = findViewById(R.id.open_flashlight);
        mZXingView.setDelegate(this);
        tvOpenFlashlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isOpen){
                    mZXingView.openFlashlight();
                    isOpen = true;
                    tvOpenFlashlight.setText("关闭闪光灯");
                }else{
                    mZXingView.closeFlashlight();
                    isOpen = false;
                    tvOpenFlashlight.setText("打开闪光灯");
                }
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_erqode;
    }

    @Override
    protected ErqodePresenter createPresenter() {
        return new ErqodePresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mZXingView.startCamera(); // 打开后置摄像头开始预览，但是并未开始识别
//        mZXingView.startCamera(Camera.CameraInfo.CAMERA_FACING_FRONT); // 打开前置摄像头开始预览，但是并未开始识别

        mZXingView.startSpotAndShowRect(); // 显示扫描框，并开始识别
    }

    @Override
    protected void onStop() {
        mZXingView.stopCamera(); // 关闭摄像头预览，并且隐藏扫描框
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mZXingView.onDestroy(); // 销毁二维码扫描控件
        super.onDestroy();
    }

    @Override
    public void onScanQRCodeSuccess(String result) {
        LogUtils.i("result:" + result);
        setTitle("扫描结果为：" + result);
        code = result;
        vibrate();
        mZXingView.stopSpot();
        if(isScan) {
           Intent intent = getIntent();
           intent.putExtra("data",code);
           setResult(OK,intent);
           this.finish();
        }else{
            MMKV kv = MMKV.defaultMMKV();
            String toekn = kv.decodeString("token");
            String userid = kv.decodeString("userid");
            if (!TextUtils.isEmpty(toekn) && !TextUtils.isEmpty(userid) && !TextUtils.isEmpty(result)) {
                mPresenter.check(toekn, userid, result);
            }
        }

    }
    private void vibrate() {
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(200);
    }
    @Override
    public void onCameraAmbientBrightnessChanged(boolean isDark) {

    }

    @Override
    public void onScanQRCodeOpenCameraError() {
        LogUtils.e( "打开相机出错");
    }

    @Override
    public void checkSuccess(CheckCodeBean userBean) {
        Intent intent = new Intent(ErQodeActivity.this, ResultActivity.class);
        intent.putExtra("result",userBean);
        intent.putExtra("code",code);
        startActivity(intent);
    }

    @Override
    public void checkError(String msg) {
        Intent intent = new Intent(ErQodeActivity.this, ResultActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
