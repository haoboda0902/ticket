package com.hbd.ticketscheck.orderstatistics.bean;

import java.io.Serializable;
import java.util.List;

public class SumOrder implements Serializable {


    /**
     * goodsList : [{"goodsNum":"5","goodsPrice":"168.0","orderNum":"4","sumPrice":"840"},{"goodsNum":"133","goodsPrice":"88.0","orderNum":"77","sumPrice":"11704"}]
     * totalMoney : 12544
     */

    private String totalMoney;
    private List<GoodsListBean> goodsList;

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    public List<GoodsListBean> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<GoodsListBean> goodsList) {
        this.goodsList = goodsList;
    }

    public static class GoodsListBean {
        /**
         * goodsNum : 5
         * goodsPrice : 168.0
         * orderNum : 4
         * sumPrice : 840
         */

        private String goodsNum;
        private String goodsPrice;
        private String orderNum;
        private String sumPrice;

        public String getGoodsNum() {
            return goodsNum;
        }

        public void setGoodsNum(String goodsNum) {
            this.goodsNum = goodsNum;
        }

        public String getGoodsPrice() {
            return goodsPrice;
        }

        public void setGoodsPrice(String goodsPrice) {
            this.goodsPrice = goodsPrice;
        }

        public String getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(String orderNum) {
            this.orderNum = orderNum;
        }

        public String getSumPrice() {
            return sumPrice;
        }

        public void setSumPrice(String sumPrice) {
            this.sumPrice = sumPrice;
        }
    }
}
