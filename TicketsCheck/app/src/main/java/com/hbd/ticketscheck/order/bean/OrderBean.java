package com.hbd.ticketscheck.order.bean;

import java.io.Serializable;
import java.util.List;

public class OrderBean implements Serializable {


    /**
     * total : 3
     * orders : [{"addressId":2,"checkDate":"","createDate":"2020-11-14 11:49:56","createUser":"1","desc":"","foreignNo":"","id":258,"openId":"123","orderMoney":"72.0","orderNo":"10RP202011141149555610090821","orderNum":6,"orderState":2,"payDate":"","postageMoney":"0","postageNo":"","provice":"北京市","proviceId":"1","receiverAddress":"北京市--北京市--丰台--中建一局10号楼","receiverMobile":"132213124","receiverName":"test","returnPtno":"1231","storeId":1,"updateDate":"2020-11-14 19:00:45","updateUser":""},{"addressId":2,"checkDate":"","createDate":"2020-11-14 11:52:16","createUser":"1","desc":"","foreignNo":"","id":259,"openId":"123","orderMoney":"72.0","orderNo":"10RP202011141152158813082763","orderNum":6,"orderState":5,"payDate":"","postageMoney":"0","postageNo":"","provice":"北京市","proviceId":"1","receiverAddress":"北京市--北京市--丰台--中建一局10号楼","receiverMobile":"132213124","receiverName":"test","returnPtno":"","storeId":1,"updateDate":"2020-11-14 16:40:54","updateUser":"admin1"},{"addressId":2,"checkDate":"","createDate":"2020-11-14 11:54:12","createUser":"1","desc":"","foreignNo":"","id":260,"openId":"oGAxc5GZs480Pj1miuKLFBdOpNlQ","orderMoney":"72.0","orderNo":"10RP202011141154118472974995","orderNum":6,"orderState":7,"payDate":"","postageMoney":"0","postageNo":"","provice":"北京市","proviceId":"1","receiverAddress":"北京市--北京市--丰台--中建一局10号楼","receiverMobile":"132213124","receiverName":"test","returnPtno":"","storeId":1,"updateDate":"2020-11-14 16:40:47","updateUser":"admin1"}]
     */

    private int total;
    private List<OrdersBean> orders;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<OrdersBean> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersBean> orders) {
        this.orders = orders;
    }

    public static class OrdersBean implements Serializable{
        /**
         * addressId : 2
         * checkDate :
         * createDate : 2020-11-14 11:49:56
         * createUser : 1
         * desc :
         * foreignNo :
         * id : 258
         * openId : 123
         * orderMoney : 72.0
         * orderNo : 10RP202011141149555610090821
         * orderNum : 6
         * orderState : 2
         * payDate :
         * postageMoney : 0
         * postageNo :
         * provice : 北京市
         * proviceId : 1
         * receiverAddress : 北京市--北京市--丰台--中建一局10号楼
         * receiverMobile : 132213124
         * receiverName : test
         * returnPtno : 1231
         * storeId : 1
         * updateDate : 2020-11-14 19:00:45
         * updateUser :
         */

        private int addressId;
        private String checkDate;
        private String createDate;
        private String createUser;
        private String desc;
        private String foreignNo;
        private int id;
        private String openId;
        private String orderMoney;
        private String orderNo;
        private int orderNum;
        private int orderState;
        private String payDate;
        private String postageMoney;
        private String postageNo;
        private String provice;
        private String proviceId;
        private String receiverAddress;
        private String receiverMobile;
        private String receiverName;
        private String returnPtno;
        private int storeId;
        private String updateDate;
        private String updateUser;

        public int getAddressId() {
            return addressId;
        }

        public void setAddressId(int addressId) {
            this.addressId = addressId;
        }

        public String getCheckDate() {
            return checkDate;
        }

        public void setCheckDate(String checkDate) {
            this.checkDate = checkDate;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getCreateUser() {
            return createUser;
        }

        public void setCreateUser(String createUser) {
            this.createUser = createUser;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getForeignNo() {
            return foreignNo;
        }

        public void setForeignNo(String foreignNo) {
            this.foreignNo = foreignNo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOpenId() {
            return openId;
        }

        public void setOpenId(String openId) {
            this.openId = openId;
        }

        public String getOrderMoney() {
            return orderMoney;
        }

        public void setOrderMoney(String orderMoney) {
            this.orderMoney = orderMoney;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public int getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(int orderNum) {
            this.orderNum = orderNum;
        }

        public int getOrderState() {
            return orderState;
        }

        public void setOrderState(int orderState) {
            this.orderState = orderState;
        }

        public String getPayDate() {
            return payDate;
        }

        public void setPayDate(String payDate) {
            this.payDate = payDate;
        }

        public String getPostageMoney() {
            return postageMoney;
        }

        public void setPostageMoney(String postageMoney) {
            this.postageMoney = postageMoney;
        }

        public String getPostageNo() {
            return postageNo;
        }

        public void setPostageNo(String postageNo) {
            this.postageNo = postageNo;
        }

        public String getProvice() {
            return provice;
        }

        public void setProvice(String provice) {
            this.provice = provice;
        }

        public String getProviceId() {
            return proviceId;
        }

        public void setProviceId(String proviceId) {
            this.proviceId = proviceId;
        }

        public String getReceiverAddress() {
            return receiverAddress;
        }

        public void setReceiverAddress(String receiverAddress) {
            this.receiverAddress = receiverAddress;
        }

        public String getReceiverMobile() {
            return receiverMobile;
        }

        public void setReceiverMobile(String receiverMobile) {
            this.receiverMobile = receiverMobile;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getReturnPtno() {
            return returnPtno;
        }

        public void setReturnPtno(String returnPtno) {
            this.returnPtno = returnPtno;
        }

        public int getStoreId() {
            return storeId;
        }

        public void setStoreId(int storeId) {
            this.storeId = storeId;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(String updateUser) {
            this.updateUser = updateUser;
        }
    }
}
