package com.hbd.network.apimanager;

import android.text.TextUtils;

import com.tencent.mmkv.MMKV;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        builder.addHeader("token","");

        MMKV kv = MMKV.defaultMMKV();
        String toekn = kv.decodeString("token");
        String userid = kv.decodeString("userid");
        if(!TextUtils.isEmpty(toekn) && !TextUtils.isEmpty(userid)) {
            Request request = chain.request();
            if ("POST".equals(request.method())) {

                HttpUrl url = request.url();
                HttpUrl newUrl = url.newBuilder()
                        .addEncodedQueryParameter("token", toekn)
                        .addEncodedQueryParameter("userid", userid)
                        .build();
                request = request.newBuilder()
                        .url(newUrl).build();
            }
            return chain.proceed(request);
        }
       return  chain.proceed(builder.build());
    }
}
