package com.hbd.network.basepresenter;

import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.baseview.IView;

import io.reactivex.Observable;

public interface Ipresenter<V extends IView> {

    /**
     * 关联P与V
     * @param v
     */
    void attachView(V v);

    /**
     * 取消关联P与V
     */
    void detachView();

    /**
     * Rx订阅
     */
    void subscribe(Observable<?> observable, BaseObserver observer);

    /**
     * Rx取消订阅
     */
    void unSubscribe();

}
