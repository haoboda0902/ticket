package com.hbd.network.apimanager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hbd.network.api.Api;
import com.hbd.network.config.DirConfig;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {

    private static ApiManager apiManager;


    private OkHttpClient mOkHttpClient;

    private final int TIME_OUT = 30;

    public ApiManager() {
        initOkHttpClient();
    }

    public static  ApiManager getApi(){

        if(apiManager == null){
            synchronized (ApiManager.class){
                if(apiManager == null){
                    apiManager = new ApiManager();
                }
            }
        }
        return apiManager;
    }


    private void initOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(new HeaderInterceptor())
                .addInterceptor(new NetCacheInterceptor());

        addLogIntercepter(builder);  //日志拦截器
        setCacheFile(builder);  //网络缓存
        mOkHttpClient = builder.build();
    }


    public  Retrofit  initRetrofit() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.SERVER_ADDRESS)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(mOkHttpClient)
                .build();
        return retrofit;
    }

    /**
     * 设置缓存文件路径
     */
    private void setCacheFile(OkHttpClient.Builder builder) {
        //设置缓存文件
        File cacheFile = new File(DirConfig.HTTP_CACHE);
        //缓存大小为10M
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(cacheFile,cacheSize);
        builder.cache(cache);
    }

    /**
     * 调试模式下加入日志拦截器
     * @param builder
     */
    private void addLogIntercepter(OkHttpClient.Builder builder) {
        if (Api.isDebug) {
            builder.addInterceptor(new LoggingInterceptor());
        }
    }

}
