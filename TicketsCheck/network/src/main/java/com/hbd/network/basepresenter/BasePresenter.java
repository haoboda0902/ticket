package com.hbd.network.basepresenter;

import com.hbd.network.apimanager.BaseObserver;
import com.hbd.network.baseview.IBaseView;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public abstract class BasePresenter<V extends IBaseView>  implements Ipresenter<V>{

    private CompositeDisposable compositeDisposable;
    public V iView;


    @Override
    public abstract void attachView(V v);

    @Override
    public void detachView() {
        iView = null;
        unSubscribe();
    }

    @Override
    public void subscribe(Observable<?> observable, BaseObserver observer) {

        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer));

    }

    @Override
    public void unSubscribe() {
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
        }
    }
}
