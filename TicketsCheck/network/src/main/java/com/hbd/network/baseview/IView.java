package com.hbd.network.baseview;

public interface IView {

    void showLoading();

    void hideLoading();

}
