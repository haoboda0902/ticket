package com.hbd.network.apimanager;

import android.util.Log;

import com.hbd.network.LogUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.hbd.network.LogUtils.TAG;

class LoggingInterceptor implements Interceptor {

    private final int byteCount = 1024*1024;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);

        LogUtils.d("请求URL：  "+request.url());

        LogUtils.d("请求数据：  "+request.body());
        String method=request.method();
        if("POST".equals(method)){
            StringBuilder sb = new StringBuilder();
            if (request.body() instanceof FormBody) {
                FormBody body = (FormBody) request.body();
                for (int i = 0; i < body.size(); i++) {
                    sb.append(body.encodedName(i) + "=" + body.encodedValue(i) + ",");
                }
                String result = null;
                      try {
                               result = new String(sb.toString().getBytes("UTF-8"), "UTF-8");
                          Log.d(TAG, "| RequestParams:{"+result+"}");
                             } catch (UnsupportedEncodingException e) {
                                 // TODO Auto-generated catch block
                                 e.printStackTrace();
                           }

//                sb.delete(sb.length() - 1, sb.length());

            }
        }

        ResponseBody responseBody = response.peekBody(byteCount);

        LogUtils.d("请求数据：  "+responseBody.string());

        return response;
    }
}
