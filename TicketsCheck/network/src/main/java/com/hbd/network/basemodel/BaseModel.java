package com.hbd.network.basemodel;

import android.text.TextUtils;

import com.hbd.network.config.AppNetConfig;

import java.io.Serializable;

public class BaseModel<T> implements IModel, Serializable {

    /** 自定义错误码 */
    public String returnCode;
    /** 自定义业务状态码 */
    public String requestStatus;
    /** 消息提示 */
    public String returnTip;
    /** 泛型实体类 */
    public T body;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getReturnTip() {
        return returnTip;
    }

    public void setReturnTip(String returnTip) {
        this.returnTip = returnTip;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    @Override
    public boolean isOK() {
        return TextUtils.equals(returnCode, AppNetConfig.SUCCESS);
    }
}
