package com.hbd.network.basemodel;

/**
 * 是否返回成功接口
 */
public interface IModel {


    boolean isOK();
}
