package com.hbd.network.apimanager;

import com.hbd.network.basemodel.BaseModel;
import com.hbd.network.baseview.IBaseView;
import io.reactivex.observers.DisposableObserver;

public abstract class BaseObserver<M> extends DisposableObserver<M> {


    private  IBaseView view;

    public BaseObserver(IBaseView view) {
        this.view = view;
    }

    @Override
    public void onNext(M m) {
        if(view != null){
            view.hideLoading();
        }
        if(m != null){
            BaseModel baseModel = (BaseModel) m;
            if(baseModel.isOK()) {
                onSuccess(m);
            }else {
                onError(baseModel.returnTip);
            }
        }
    }

    public abstract void onSuccess(M m);

    public abstract void onError(String msg);

    @Override
    public void onError(Throwable e) {
        if(view != null){
            view.hideLoading();
        }

        onError(e.toString());
    }

    @Override
    public void onComplete() {

    }
}
