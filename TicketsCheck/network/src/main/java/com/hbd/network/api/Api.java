package com.hbd.network.api;


public interface Api {

    boolean isDebug = true;

    /** 正式服务器地址 */
    String SERVER_ADDRESS_RELEASE = "http://scenicspot.bjyidongtianqi.com";

    /** 测试服务器地址 */
//    String SERVER_ADDRESS_DEBUG = "http://scenicspot.bjyidongtianqi.com";
    String SERVER_ADDRESS_DEBUG = "https://hssl.bjyidongtianqi.com/";

    /** 服务器域名 */
    String SERVER_ADDRESS = isDebug ? SERVER_ADDRESS_DEBUG : SERVER_ADDRESS_RELEASE;




}
